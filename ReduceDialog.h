#ifndef _REDUCEWINDOW_
#define _REDUCEWINDOW_

class ReduceDialog;

#include "channel3.h"
#include "PlotBox.h"

class ReduceDialog : public wxFrame{
	public:
		ReduceDialog(wxWindow *parent, PlotBox* plot);
		PlotBox *plot;
		wxPanel *panel;
		wxTextCtrl *avgText;
		wxTextCtrl *devText;
		wxSpinCtrl *startInput;
		wxSpinCtrl *endInput;

		double avg, sd;
		int startMarker;
		int endMarker;
		int startPos, endPos;

		void OnOK(wxCommandEvent& event);
		void OnMarkerChange(wxSpinEvent& event);
		void updateSpinners();
		void updateStats();
		void OnSave(wxCommandEvent& event);

		DECLARE_EVENT_TABLE();
};

#endif
