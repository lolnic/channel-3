#include "ConfigurationWindow.h"
#include <sstream>
#include <iostream>

enum{
	ID_Done=1
};

BEGIN_EVENT_TABLE(ConfigurationWindow, wxDialog)
	EVT_BUTTON(ID_Done, ConfigurationWindow::OnDoneButton)
END_EVENT_TABLE();

bool ConfigurationWindow::allEntriesAreValid(){
	wxTextCtrl* floats[] = {gainfactorInput, intervalInput, displayMaxInput, displayMinInput};
	wxTextCtrl* ints[] = {databitsInput, headerSizeInput};
	double dummydouble;
	long dummylong;
	for(int i = 0; i < sizeof(floats) / sizeof(floats[0]); i++)
		if(!floats[i]->GetValue().ToDouble(&dummydouble))return false;
	for(int i = 0; i < sizeof(ints) / sizeof(ints[0]); i++)
		if(!ints[i]->GetValue().ToLong(&dummylong))return false;
	return true;
}

void ConfigurationWindow::OnDoneButton(wxCommandEvent& WXUNUSED(event)){
	if(!allEntriesAreValid()){
		wxMessageBox(_("Not all entered values are valid numbers. "
			       "Note that you cannot have a non-integer value for \"Data bits\""),
			       _("Invalid Entries"));
		return;
	}
	gainfactorInput->GetValue().ToDouble(&gainfactor);;
	intervalInput->GetValue().ToDouble(&interval);
	displayMaxInput->GetValue().ToDouble(&displayMax);
	displayMinInput->GetValue().ToDouble(&displayMin);
	databitsInput->GetValue().ToLong(&databits);;
	signedData = signeddataCheck->GetValue();
	headerSizeInput->GetValue().ToLong(&headerSize);
	adcminInput->GetValue().ToDouble(&adcMin);
	adcmaxInput->GetValue().ToDouble(&adcMax);
	msb = bitorderBrowser->GetSelection();
	EndModal(0);
	Hide();
     plot->SetFocus();
}


ConfigurationWindow::ConfigurationWindow(wxWindow* parent, PlotBox* plot, int flag) :
	wxDialog(parent, -1, _("Sample Properties"), wxDefaultPosition, wxDefaultSize, wxCAPTION),
	displayMax(plot->displayMax), displayMin(plot->displayMin), plot(plot)
{
	fs = plot->flagToFile(flag);
	gainfactor = fs->gainfactor;
	interval = fs->interval;
	databits = fs->databits;
	signedData = fs->signedData;
	signExtend = fs->signExtend;
	msb = fs->msb;
	adcMin = fs->adcMin;
	adcMax = fs->adcMax;
	headerSize =fs->headerSize;
	wxFlexGridSizer* sizer = new wxFlexGridSizer(4, 5, 1);
	wxPanel *panel = new wxPanel(this);
	panel->SetSizer(sizer);
	wxBoxSizer* mainsizer = new wxBoxSizer(wxHORIZONTAL);
	mainsizer->Add(panel);
	SetSizer(mainsizer);


	wxSizerFlags leftlabelpos = wxSizerFlags().Align(wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
	wxSizerFlags leftentrypos = wxSizerFlags(1).Align(wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL);
	wxSizerFlags rightlabelpos = wxSizerFlags(2).Align(wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
	wxSizerFlags rightentrypos = wxSizerFlags(3).Align(wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL);

	// Create the widgets on the first row
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("Gain Factor")), leftlabelpos);
	sizer->Add(gainfactorInput = new wxTextCtrl(panel, wxID_ANY), leftentrypos);
	*gainfactorInput << gainfactor;

	sizer->Add(new wxStaticText(panel, wxID_ANY, _("")), rightlabelpos);
	sizer->Add(signeddataCheck = new wxCheckBox(panel, wxID_ANY, _("Signed Data")), rightentrypos);
	signeddataCheck->SetValue(signedData);

	// Create the widgets on the second row
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("Sample Interval")), leftlabelpos);
	sizer->Add(intervalInput = new wxTextCtrl(panel, wxID_ANY), leftentrypos);
	*intervalInput << interval;

	sizer->Add(new wxStaticText(panel, wxID_ANY, _("Header Size")), leftlabelpos);
	sizer->Add(headerSizeInput = new wxTextCtrl(panel, wxID_ANY), leftentrypos);
	*headerSizeInput << headerSize;

	// Create the widgets on the third row
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("Display Maximum")), leftlabelpos);
	sizer->Add(displayMaxInput = new wxTextCtrl(panel, wxID_ANY), leftentrypos);
	*displayMaxInput << displayMax;

	wxString Choices[] = {_("LSB"), _("MSB")};
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("Byte Order")), rightlabelpos);
	sizer->Add(bitorderBrowser = new wxComboBox(panel, wxID_ANY, _(""), wxDefaultPosition, wxSize(-1, -1), 2, Choices, wxCB_READONLY), rightentrypos);
	bitorderBrowser->SetSelection(msb);

	// Create the widgets on the fourth row
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("Display Minimum")), leftlabelpos);
	sizer->Add(displayMinInput = new wxTextCtrl(panel, wxID_ANY), leftentrypos);
	*displayMinInput << displayMin;

	sizer->Add(new wxStaticText(panel, wxID_ANY, _("ADC Maximum")), rightlabelpos);
	sizer->Add(adcmaxInput = new wxTextCtrl(panel, wxID_ANY), rightentrypos);
	*adcmaxInput << adcMax;

	// Create the widgets on the fifth row
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("Data Bits")), leftlabelpos);
	sizer->Add(databitsInput = new wxTextCtrl(panel, wxID_ANY), leftentrypos);
	*databitsInput << databits;

	sizer->Add(new wxStaticText(panel, wxID_ANY, _("ADC Minimum")), rightlabelpos);
	sizer->Add(adcminInput = new wxTextCtrl(panel, wxID_ANY), rightentrypos);
	*adcminInput << adcMin;

	for(int i = 0; i < 3; i++)sizer->AddStretchSpacer();
	sizer->Add(doneButton = new wxButton(panel, ID_Done, _("OK")), rightentrypos);

	Fit();
	Center();
	ShowModal();

}
