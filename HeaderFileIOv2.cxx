#include "HeaderFileIOv2.h"
#include "versions.h"

void HeaderFileIOv2::loadConfig(PlotBox* plot, int binaryLength, int flag) {
	FileStruct* fstruct = plot->flagToFile(flag);
	fstruct->headerSize = 2048;
	float* header = (float*)(fstruct->fileBytes + strlen(versions[0]));
	int pos = 0;
	fstruct->gainfactor = header[pos++];
	fstruct->interval = header[pos++];
	plot->displayMax = header[pos++];
	plot->displayMin = header[pos++];
	fstruct->databits = header[pos++];
	fstruct->signedData = (header[pos++] > 500.0);
	fstruct->msb = (header[pos++] > 500.0);
	int* markerHeader = (int*)&header[pos];
	pos = 0;
	if (flag == USE_STANDARD_FSTRUCT) {
		int nMarkers = markerHeader[pos++];
		for (int i = 0; i < nMarkers; i++) {
			plot->horizontalMarkers.push_back(markerHeader[pos++]);
		}
	}

	plot->verticalViewSize = (plot->displayMax - plot->displayMin);
	plot->setPlotMin(plot->displayMin);
}

int HeaderFileIOv2::read(PlotBox* plot, int binaryLength, int flag) {
	plot->parseFileContents(flag);
	return plot->flagToFile(flag)->headerSize;
}

// Currently will only write the standard file
void HeaderFileIOv2::write(PlotBox* plot, wxString filename, bool append, std::vector<short>& data, int flag) {
	if (flag == USE_ALT_FSTRUCT) {
		wxMessageBox(_("You cannot write the alternate file. Additionally, this should never be displayed."));
		return;
	}
	wxString mode = _("wb");
	if (append) {
		mode = _("ab");
	}
	wxFFile out(filename, mode);
	if(!out.IsOpened()) return;

	if (!append) {
		// Write identifier
		out.Write(versions[0], strlen(versions[0]));
		// Write header
		int nMarkers = plot->horizontalMarkers.size();
		if (nMarkers >= ((2048 - strlen(versions[0])) - 7*sizeof(float)) / sizeof(int)) {
			wxMessageBox(_("Not all markers will be saved"));
			nMarkers = ((2048 - strlen(versions[0])) - 7*sizeof(float)) / sizeof(int) - sizeof(int);
		}
		int pos = 0;
		char *headerBuffer = new char[2048 - strlen(versions[0])];
		float* header = (float*)headerBuffer;
		header[pos++] = plot->standardFile.gainfactor;
		header[pos++] = plot->standardFile.interval * plot->dropOut;
		header[pos++] = plot->displayMax;
		header[pos++] = plot->displayMin;
		header[pos++] = plot->standardFile.databits;
		header[pos++] = (plot->standardFile.signedData)?(1000.0):(0.0);
		header[pos++] = (plot->standardFile.msb)?(1000.0):(0.0);
		int* markerHeader = (int*)&header[pos];
		pos = 0;
		markerHeader[pos++] = nMarkers;
		for (int i = 0; i < nMarkers; i++) {
			markerHeader[pos++] = plot->horizontalMarkers[i];
		}
		out.Write(header, 2048 - strlen(versions[0]));
		delete[] headerBuffer;
	}

	// Write data
	char* actualBuffer = new char[data.size()*2/plot->dropOut];

	for(int i = 0; i < data.size(); i+=plot->dropOut) {
		int tmp = data[i];
		char byte1, byte2;
		if (tmp < 0) {
			tmp = -tmp;
			tmp--;
			byte1 = ~(tmp / 256);
			byte2 = ~(tmp % 256);
		} else {
			byte1 = tmp / 256;
			byte2 = tmp % 256;
		}
		if (!plot->standardFile.msb) {
			char tmp = byte1;
			byte1 = byte2;
			byte2 = tmp;
		}
		actualBuffer[i*2/plot->dropOut] = byte1;
		actualBuffer[i*2/plot->dropOut+1] = byte2;
	}
	out.Write(actualBuffer, data.size() * 2 / plot->dropOut);
	delete[] actualBuffer;

	out.Close();
}
