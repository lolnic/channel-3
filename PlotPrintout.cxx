#include "PlotPrintout.h"
#include <wx/log.h>
#include <wx/utils.h>
#include <wx/printdlg.h>
#include <algorithm>
using std::min;

PlotPrintout::PlotPrintout(wxString& fname, PlotBox* plot, int ppl, int lpp, int start, int end, int dropout, wxString comment, wxString title) :
  	wxPrintout(fname), fname(fname), plot(plot), pointsPerLine(ppl), linesPerPage(lpp), start(start), end(end), dropout(dropout), comment(comment), title(title) {
		nPages = -1;
		m_orient = wxPORTRAIT; // wxPORTRAIT, wxLANDSCAPE
        m_paper_type = wxPAPER_LETTER;
        m_margin_left   = 10;
        m_margin_right  = 10;
        m_margin_top    = 10;
        m_margin_bottom = 10;
        m_units_per_cm = 500;
}

void PlotPrintout::copyPageSetup(PlotPrintout& other) {
	m_margin_left = other.m_margin_left;
	m_margin_right = other.m_margin_right;
	m_margin_bottom = other.m_margin_bottom;
	m_margin_top = other.m_margin_top;
	m_orient = other.m_orient;
	m_paper_type = other.m_paper_type;
	m_page_setup = other.m_page_setup;
}

bool PlotPrintout::performPageSetup(const bool showPageSetupDialog) {
    // don't show page setup dialog, use default values
    wxPrintData printdata;
    printdata.SetPrintMode( wxPRINT_MODE_PRINTER );
    printdata.SetOrientation( m_orient );
    printdata.SetNoCopies(1);
    printdata.SetPaperId( m_paper_type ); 
    
    m_page_setup = wxPageSetupDialogData(printdata);
    m_page_setup.SetMarginTopLeft    (wxPoint(m_margin_left,  m_margin_top));
    m_page_setup.SetMarginBottomRight(wxPoint(m_margin_right, m_margin_bottom)); 
    
    if (false)
    {
        wxPageSetupDialog dialog( NULL, &m_page_setup );
        if (dialog.ShowModal() == wxID_OK)
        {
            
            m_page_setup = dialog.GetPageSetupData();
            m_orient = m_page_setup.GetPrintData().GetOrientation();
            m_paper_type = m_page_setup.GetPrintData().GetPaperId();
            
            wxPoint marginTopLeft = m_page_setup.GetMarginTopLeft();
            wxPoint marginBottomRight = m_page_setup.GetMarginBottomRight();
            m_margin_left   = marginTopLeft.x;
            m_margin_right  = marginBottomRight.x;
            m_margin_top    = marginTopLeft.y;
            m_margin_bottom = marginBottomRight.y;
        }
        else
        {
            return false;
        }
    }
    return true;
}

wxPrintData PlotPrintout::getPrintData()
{
	return m_page_setup.GetPrintData();
}

void PlotPrintout::GetPageInfo(int *minPage, int *maxPage, int *pageFrom, int *pageTo) {
	*minPage = 1;
	*maxPage = nPages;
	*pageFrom = 1;
	*pageTo = nPages;
}

bool PlotPrintout::OnBeginDocument(int startPage, int endPage) {
	return wxPrintout::OnBeginDocument(startPage, endPage);
}

void PlotPrintout::OnBeginPrinting() {
	wxSize paperSize = m_page_setup.GetPaperSize();  // in millimeters

    // still in millimeters
	float large_side = std::max(paperSize.GetWidth(), paperSize.GetHeight());
	float small_side = std::min(paperSize.GetWidth(), paperSize.GetHeight());

    float large_side_cm = large_side / 10.0f;  // in centimeters
    float small_side_cm = small_side / 10.0f;  // in centimeters
    
    if (m_orient == wxPORTRAIT)
    {
    	float ratio = float(large_side - m_margin_top  - m_margin_bottom) /
    	float(small_side - m_margin_left - m_margin_right);

    	m_coord_system_width  = (int)((small_side_cm - m_margin_left/10.f -
    		m_margin_right/10.0f)*m_units_per_cm);
    	m_coord_system_height = m_coord_system_width*ratio;
    }
    else
    {
    	float ratio = float(large_side - m_margin_left - m_margin_right) /
    	float(small_side - m_margin_top  - m_margin_bottom);

    	m_coord_system_height = (int)((small_side_cm - m_margin_top/10.0f -
    		m_margin_bottom/10.0f)* m_units_per_cm);
    	m_coord_system_width  = m_coord_system_height*ratio;

    }
}

void PlotPrintout::OnPreparePrinting() {
	printFont = new wxFont(100, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
	int range = end-start;
	nLines = range / pointsPerLine;
	if (range%pointsPerLine != 0) nLines++;
	nPages = nLines / linesPerPage;
	if (nLines % linesPerPage != 0) nPages++;
}

bool PlotPrintout::HasPage(int pageNum) {
	if (1 <= pageNum && pageNum <= nPages) return true;
	return false;
}

wxCoord PlotPrintout::scale(wxCoord width, short smallest, short biggest, short data) {
	double result = data - smallest;
	result /= (biggest-smallest);
	result *= width;
	return (wxCoord)result;
}

void PlotPrintout::printInfo(wxDC *dc, wxString data) {
	width -= 5;
	int textw, texth;
	dc->GetTextExtent(data, &textw, &texth);
	dc->DrawRotatedText(data, width, 10, 270);
	width -= texth;
}

bool PlotPrintout::OnPrintPage (int pageNum) {
	FitThisSizeToPageMargins(wxSize(m_coord_system_width, m_coord_system_height), m_page_setup);

	wxRect fitRect = GetLogicalPageMarginsRect(m_page_setup);
	        
    wxCoord xoff = (fitRect.width - m_coord_system_width) / 2;
    wxCoord yoff = (fitRect.height - m_coord_system_height) / 2;
    OffsetLogicalOrigin(xoff, yoff);

	wxDC * dc = GetDC();
	if (dc==NULL || !dc->IsOk()) {
		wxMessageBox(_("Could not print. Device context is not Ok."));
		return false;
	}
	width  = m_coord_system_width;
	height = m_coord_system_height;
	dc->SetFont(*printFont);


	double secondsPerLine = ((double)pointsPerLine) / 1000000 * plot->standardFile.interval;
	int pageStartPoint = (pageNum-1) * linesPerPage * pointsPerLine + start;
	int pageEndPoint = min(end, (pageNum) * linesPerPage * pointsPerLine + start);
	double pageStartTime = ((double)pageStartPoint) / 1000000 * plot->standardFile.interval;
	double pageEndTime = ((double)pageEndPoint) / 1000000 * plot->standardFile.interval;

	wxCoord signalWidth = width/(linesPerPage*1.2+1);
	wxCoord lastx, lasty;
	short biggest, smallest;
	biggest = plot->fromVoltage(plot->getPlotMin() + plot->getPlotRange());
	smallest = plot->fromVoltage(plot->getPlotMin());
	int printedLines = linesPerPage * (pageNum-1);
	int linesThisPage = linesPerPage;
	if (printedLines + linesPerPage > nLines) {
		linesThisPage = nLines - printedLines;
	}
	float highvolt = plot->toVoltage(biggest);
	float lowvolt = plot->toVoltage(smallest);
		
	wxString line;
	line.Printf(_("Page %d of %d"), pageNum, nPages);
	printInfo(dc, line);
	line.Printf(_("Title: %s"), title);
	printInfo(dc, line);
	line.Printf(_("File: %s"), fname);
	printInfo(dc, line);
	line.Printf(_("Seconds per line: %.2f"), secondsPerLine);
	printInfo(dc, line);
	line.Printf(_("Time: %.2f     -     %.2f"), pageStartTime, pageEndTime);
	printInfo(dc, line);
	line.Printf(_("Voltage range: %.2f     -     %.2f"), lowvolt, highvolt);
	printInfo(dc, line);
	if (comment != _("")) printInfo(dc, comment);
	
	int margin = 0;

	for (int j = 0; j < linesThisPage; j++) {
		int whichLine = linesPerPage - j - 1;
		dc->SetPen(*wxBLACK_PEN);
		int startPoint = (pageNum-1) * linesPerPage * pointsPerLine + start + j*pointsPerLine;
		int endPoint = (pageNum-1) * linesPerPage * pointsPerLine + start + (j+1)*pointsPerLine;
		int p = plot->standardFile.dataPoints[startPoint];
		if (p < smallest) p = smallest;
		if (p > biggest) p = biggest;
		lastx = width*(whichLine*2+1)/(linesPerPage*2+1)+scale(signalWidth, smallest, biggest, p);
		lasty = margin;
		dc->DrawLine(width*(whichLine*2+1)/(linesPerPage*2+1)+scale(signalWidth, smallest, biggest, smallest), margin/2,
				       width*(whichLine*2+1)/(linesPerPage*2+1)+scale(signalWidth, smallest, biggest, biggest), margin/2);
		for (int i = startPoint+1; i < min(end, endPoint); i += dropout) {
			int p = plot->standardFile.dataPoints[i];
			if (p < smallest) p = smallest;
			if (p > biggest) p = biggest;
			wxCoord x = width*(whichLine*2+1)/(linesPerPage*2+1)+scale(signalWidth, smallest, biggest, p);
			wxCoord y = (i-startPoint+1)/(double)(endPoint-startPoint)*(height-2*margin)+margin;
			dc->DrawLine(lastx, lasty, x, y);
			lastx=x;
			lasty=y;
		}
		if (biggest > 0 && smallest < 0) {
			// then draw a dotted line at zero
			dc->SetPen(*wxBLACK_DASHED_PEN);
			wxCoord linex = width*(whichLine*2+1)/(linesPerPage*2+1)+scale(signalWidth, smallest, biggest, 0);
			dc->DrawLine(linex, 0, linex, height);
		}
	}


	return true;
}
