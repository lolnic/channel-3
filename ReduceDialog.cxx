#include "ReduceDialog.h"
#include "CustomDialog.h"

enum{
	ID_average,
	ID_deviation,
};

BEGIN_EVENT_TABLE(ReduceDialog, wxFrame)
	EVT_BUTTON(wxID_OK, ReduceDialog::OnOK)
	EVT_BUTTON(wxID_CANCEL, ReduceDialog::OnSave)
	EVT_SPINCTRL(wxID_ANY, ReduceDialog::OnMarkerChange)
END_EVENT_TABLE()

void ReduceDialog::OnOK(wxCommandEvent& event){
	Show(false);
}

void ReduceDialog::OnSave(wxCommandEvent& event) {
	wxString outStr;
	wxString fstats;
	wxString fheadings = _("Average current\tS.D.\tMarkers\tMarker times (ms)\n");
	wxString mode = _("w");

	fstats.Printf(_("%.2f\t\t%.2f\t%d-%d\t%.2f-%.2f\n"), avg, sd, startMarker, endMarker,
		startPos * plot->standardFile.interval / 1000,
		endPos * plot->standardFile.interval / 1000);

	outStr = fheadings + fstats;

	wxFileName fname;
	wxString path;
	wxFileDialog fileChooser(NULL, _("Choose where to save"), wxGetApp().lastFolder, _(""), _("All Files (*)|*"), wxFD_SAVE);

	switch(fileChooser.ShowModal()) {
		case wxID_CANCEL:
			return;
		default:
			path = fileChooser.GetPath();
	}
	if (fname.FileExists(path)) {
		wxString appendChoices[] = {_("Overwrite"), _("Append")};
		if (getCustomDlgChoice(NULL, _("Do you want to append to existing file?"), _("File Exists"), 2, appendChoices) == 1) {
			outStr = fstats;
			mode = _("a");
		}
	}

	wxFFile out2(path, mode);
	out2.Write(outStr);
	out2.Close();
}

void ReduceDialog::updateSpinners() {
	int startVal = startInput->GetValue();
	int endVal = endInput->GetValue();
	startInput->SetRange(0, endVal);
	endInput->SetRange(startVal+1, plot->horizontalMarkers.size()+1);
}

void ReduceDialog::updateStats() {
	startMarker = startInput->GetValue();
	endMarker = endInput->GetValue();
	if (startMarker == 0) startPos = 0;
	else startPos = plot->horizontalMarkers[startMarker-1];
	if (endMarker == plot->horizontalMarkers.size()+1) endPos = plot->standardFile.dataPoints.size();
	else endPos = plot->horizontalMarkers[endMarker-1];
	float sumX = 0;
	float sumSquare = 0;
	for (int i = startPos; i < endPos; i++) {
		float voltage = ((float)plot->standardFile.dataPoints[i] * 20 * 1000) / 
			(((1 << plot->standardFile.databits)-1) * plot->standardFile.gainfactor);
		sumX += voltage;
		sumSquare += voltage*voltage;
	}
	avg = sumX / (endPos-startPos) - plot->verticalMarkers[1];
	sd = sqrt(sumSquare/(endPos-startPos) - pow(sumX / (endPos-startPos), 2));
	avgText->SetValue(_(""));
	devText->SetValue(_(""));
	*avgText << avg;
	*devText << sd;
}

void ReduceDialog::OnMarkerChange(wxSpinEvent& event) {
	updateSpinners();
	updateStats();
}

ReduceDialog::ReduceDialog(wxWindow *parent, PlotBox *plot)
	: wxFrame(parent, -1, _("Statistics"), wxDefaultPosition, wxDefaultSize,
			wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxSYSTEM_MENU | wxCAPTION | wxCLIP_CHILDREN),
	plot(plot)
{
	panel = new wxPanel(this, wxID_ANY);
	wxFlexGridSizer *sizer = new wxFlexGridSizer(2, 5, 10);

	sizer->SetSizeHints(panel);
	panel->SetSizer(sizer);
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("Average: ")), 1, wxLEFT|wxTOP, 10);
	sizer->Add(avgText = new wxTextCtrl(panel, wxID_ANY, _("...")), 0, wxRIGHT|wxTOP, 10);
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("Std. Dev: ")), 1, wxLEFT, 10);
	sizer->Add(devText = new wxTextCtrl(panel, wxID_ANY, _("...")), 0, wxRIGHT, 10);
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("From")), 1, wxLEFT, 10);
	sizer->Add(startInput = new wxSpinCtrl(panel, wxID_ANY, _("0")), 0, wxRIGHT, 10);	
	sizer->Add(new wxStaticText(panel, wxID_ANY, _("To")), 1, wxLEFT, 10);
	sizer->Add(endInput = new wxSpinCtrl(panel, wxID_ANY, _("1")), 0, wxRIGHT, 10);
	avgText->SetEditable(false);
	devText->SetEditable(false);
	updateSpinners();
	updateStats();

	wxStdDialogButtonSizer *buttonSizer = new wxStdDialogButtonSizer();
	buttonSizer->AddButton(new wxButton(panel, wxID_OK, _("OK")));
	buttonSizer->AddButton(new wxButton(panel, wxID_CANCEL, _("Save")));
	buttonSizer->Realize();
	sizer->Add(buttonSizer, 0, wxSHAPED|wxBOTTOM|wxTOP, 5);

	panel->Fit();
	Fit();
	Centre();
}

