#include "PlotPanel.h"
#include "channel3.h"

enum {
  ID_Vscroll=1,
  ID_ZoomVertical,
  ID_Scroll
};

BEGIN_EVENT_TABLE(PlotPanel, wxPanel)
  EVT_COMMAND_SCROLL(ID_Vscroll, PlotPanel::OnVScrollMove)
  EVT_COMMAND_SCROLL(ID_Scroll, PlotPanel::OnScrollbarMove)
  EVT_COMMAND_SCROLL(ID_ZoomVertical, PlotPanel::OnZoomVertical)
END_EVENT_TABLE();

PlotPanel::PlotPanel(
  wxWindow* parent,
  wxWindowID id,
  wxScrollBar* fileScrollbar,
  const wxPoint &pos,
  const wxSize &size,
  long style,
  const wxString &name
): wxPanel(parent, id, pos, size, style, name) {
  // Create a PlotBox
	wxGLAttributes gl_attrib;
  #ifdef __WXMSW__
	  gl_attrib.PlatformDefaults().EndList();
  #else
	  gl_attrib.PlatformDefaults().RGBA().MinRGBA(1, 1, 1, 1).Depth(1).DoubleBuffer().EndList();
  #endif /* __WXMSW__ */
	plot = new PlotBox(this, wxID_ANY, gl_attrib, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE);
	//plot->setDropoutInput (dropoutInput);

  // Create two ScaleBars
	verticalScale = new ScaleBar(this, wxID_ANY, wxDefaultPosition, wxSize(50, 1), wxFULL_REPAINT_ON_RESIZE);
	horizontalScale = new ScaleBar(this, wxID_ANY, wxDefaultPosition, wxSize(1, SCALEBAR_HEIGHT), wxFULL_REPAINT_ON_RESIZE);

  // Create zoom control
  verticalZoom = new wxSlider(this, ID_ZoomVertical, 500, 500, 1000, wxDefaultPosition, wxDefaultSize, wxSL_VERTICAL|wxSL_RIGHT);

  plot->associateRefreshingScale(horizontalScale);

  wxBoxSizer* horizontalSizer = new wxBoxSizer(wxHORIZONTAL);
  this->SetSizer(horizontalSizer);
  horizontalSizer->SetSizeHints(this);

  wxBoxSizer *leftZoomSizer = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *scaleHorizontalSizer = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *plotVerticalSizer = new wxBoxSizer(wxVERTICAL);

  horizontalSizer->Add(leftZoomSizer, 0, wxEXPAND|wxBOTTOM, PLOT_MARGIN);
	horizontalSizer->Add(scaleHorizontalSizer, 0, wxEXPAND|wxBOTTOM, PLOT_MARGIN);
	horizontalSizer->Add(plotVerticalSizer, 1, wxEXPAND);

  leftZoomSizer->Add(verticalZoom, 0, wxEXPAND);

  verticalScrollbar = new wxScrollBar(this, ID_Vscroll, wxDefaultPosition, wxDefaultSize, wxSB_VERTICAL);
	scaleHorizontalSizer->Add(verticalScrollbar, 0, wxEXPAND);
	scaleHorizontalSizer->Add(verticalScale, 0, wxEXPAND);

  plotVerticalSizer->Add(plot, 1, wxEXPAND);
  plotVerticalSizer->Add(horizontalScale, 0, wxEXPAND);

  if (fileScrollbar == NULL) {
    fileScrollbar = new wxScrollBar(this, ID_Scroll,
  			wxPoint(plot->GetPosition().x-SCROLLBAR_HEIGHT, horizontalScale->GetPosition().y + horizontalScale->GetSize().GetHeight() +5),
  			wxSize(plot->GetSize().GetWidth() + SCROLLBAR_HEIGHT*2, SCROLLBAR_HEIGHT));
    plotVerticalSizer->Add(fileScrollbar, 0, wxEXPAND);
  }
  this->fileScrollbar = fileScrollbar;

  this->fileScrollbar->SetScrollbar(0, 50, 1000, 100);
	plot->associateScrollBars(this->fileScrollbar, verticalScrollbar);
	horizontalScale->associatePlotBox(plot);
	verticalScale->associatePlotBox(plot);
	verticalScale->setmode(SCALEBAR_VERTICAL);
}

PlotPanel::~PlotPanel() {
}

void PlotPanel::OnVScrollMove(wxScrollEvent& event){
	plot->saveVScrollValues();

  GetParent()->Refresh();
  GetParent()->Update();
}

void PlotPanel::OnScrollbarMove(wxScrollEvent& event){
  GetParent()->Refresh();
  GetParent()->Update();
}

void PlotPanel::OnZoomVertical(wxScrollEvent& event){
	plot->saveVScrollValues();
	int plotMin = plot->getPlotMin();
	float zoomsize = pow(2, verticalZoom->GetValue()/10.0) - plot->getPlotRange();

	plot->verticalViewSize = (std::max(10, (int)(plot->getPlotRange() + zoomsize)));
	plot->setPlotMin(std::max(-(1<<(std::max(plot->standardFile.databits, plot->altFile.databits)-1)), (int)(plotMin-zoomsize/2)));
	plot->adjustScrollbars();

	GetParent()->Refresh();
  GetParent()->Update();
}

void PlotPanel::adjustZoomControls() {
  verticalZoom->SetRange(10*4, 10*std::max(plot->standardFile.databits, plot->altFile.databits));
	verticalZoom->SetValue(log2(plot->getPlotRange())*10);
}

void PlotPanel::clearMarkers() {
  this->horizontalScale->clearMarkers();
}

int PlotPanel::getFocussedMarker() {
  return this->horizontalScale->focussedMarker;
}

void PlotPanel::removeFilters() {
  return this->plot->removeFilters();
}

PlotBox* PlotPanel::getPlot() {
  return this->plot;
}
