CXX		= $(shell wx-config --cxx )
DEBUG		= -g
CXXFLAGS	= $(shell wx-config --cxxflags ) -I. -Wall
LDFLAGS		= $(shell wx-config --libs --gl-libs ) -lGLU
LDSTATIC	= $(shell wx-config --linkdeps --gl-libs ) -static-libgcc -static-libstdc++
LINK		= $(CXX)
OBJ		= AverageFilter.o GaussianFilter.o channel3.o ConfigurationWindow.o Filter.o FilterDialog.o MainWindow.o MedianFilter.o PlotBox.o PreferencesDialog.o ScaleBar.o versions.o TabBar.o FileIO.o HeaderFileIOv2.o CustomDialog.o ReduceDialog.o PrintDialog.o PlotPrintout.o Histogram.o HistBox.o PlotBoxBaseline.o
SRC		= $(OBJ,.o=.cxx)
TARGET		= channel3
HEADERS		= *.h
OPTIMISE	= # -O3  -ffast-math

$(TARGET): $(OBJ)
	$(LINK) $(OBJ) $(LDFLAGS) -o $(TARGET) $(OPTIMISE) $(DEBUG)

%.o : %.cxx $(HEADERS)
	$(CXX) $(CXXFLAGS) -c $< $(OPTIMISE) $(DEBUG)

clean:
	rm -f $(OBJ) $(TARGET) 2> /dev/null
