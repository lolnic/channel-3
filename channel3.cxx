#include "MainWindow.h"
#include "channel3.h"
#include "config.h"

BEGIN_EVENT_TABLE(Channel3, wxApp)
     EVT_KEY_DOWN(Channel3::keyShortcut)
END_EVENT_TABLE();

void Channel3::keyShortcut(wxKeyEvent& event) {
	MainWindow* activeWin = NULL;
	for (int i = 0; i < win.size(); i++) {
		if (win[i]->IsActive()) {
			activeWin = win[i];
		}
	}
	if (activeWin == NULL) {
		event.Skip();
		return;
	}

     switch(event.GetKeyCode()) {
        case WXK_LEFT:
        	if (event.GetModifiers() == wxMOD_ALT) {
        		//activeWin->horizontalScale->shiftMarker(-1);
        	} else {
            	//activeWin->plot->scrollLeft();
            }
            break;
        case WXK_RIGHT:
        	if (event.GetModifiers() == wxMOD_ALT) {
        		//activeWin->horizontalScale->shiftMarker(1);
        	} else {
            	//activeWin->plot->scrollRight();
            }
			break;
		case WXK_UP:
			activeWin->tabBar->shiftMarker(1);
			break;
		case WXK_DOWN:
			activeWin->tabBar->shiftMarker(-1);
			break;
		case 'D':
			if(event.GetModifiers() == wxMOD_ALT) {
				activeWin->dropoutInput->SetFocus();
			} else event.Skip();
			break;
		case 'G':
			if(event.GetModifiers() == wxMOD_ALT) {
				activeWin->gotoInput->SetFocus();
			} else event.Skip();
			break;
        default:
            event.Skip();
     }
}

void Channel3::removeWindow (MainWindow* closedWin) {
	int closedIndex = -1;
	for (int i = 0; i < win.size(); i++) {
		if (win[i] == closedWin) {
			closedIndex = i;
		}
	}
	assert(closedIndex != -1);

	for (int i = closedIndex; i < win.size() -1; i++) {
		win[i] = win[i+1];
	}
	win.pop_back();
}

void Channel3::newWindow () {
	win.push_back(new MainWindow(_(FULL_APP_NAME), wxPoint(50, 50), wxSize(1300, 650)));
	win[win.size()-1]->Show(true);
	SetTopWindow(win[win.size()-1]);
}

void Channel3::MacOpenFile(const wxString &fileName) {
	int chosenWindow = -1;
	for (int i = 0; i < win.size(); i++) {
		// if (win[i]->plot->file() == _("")) {
		// 	chosenWindow = i;
		// }
	}
	if (chosenWindow == -1) {
		newWindow();
		chosenWindow = win.size() - 1;
	}

	//win[chosenWindow]->plot->openFile(fileName);

	// if(win[chosenWindow]->plot->file() != _("")){
	// 	wxString name;
	// 	wxFileName tmp;
	// 	tmp.SplitPath(win[chosenWindow]->plot->file(), NULL, NULL, &name, NULL);
	// 	win[chosenWindow]->SetTitle(name + _(" - ") + _(FULL_APP_NAME));
	// }

	win[chosenWindow]->adjustZoomControls();
	win[chosenWindow]->Refresh();
	win[chosenWindow]->Update();
}

bool Channel3::OnInit(){
	SetAppName(_(FULL_APP_NAME));

	aboutDlgInfo = new wxAboutDialogInfo();
	aboutDlgInfo->SetName(_(APP_NAME));
	aboutDlgInfo->SetVersion(_(APP_VERSION));
	aboutDlgInfo->SetWebSite(_("http://niclaver.com/channel"));
	aboutDlgInfo->AddDeveloper(_("Nicholas Laver"));
	aboutDlgInfo->SetCopyright(_("Copyright (C) 2011 Nicholas Laver"));
	aboutDlgInfo->SetDescription(_(FULL_APP_NAME " is a program designed to reproduce the functionality introduced with Channel 2 "
	                               "with the addition of a graphical use interface"));

#ifndef __WXMAC__
	if (argc == 2) {
		newWindow();
		win[win.size()-1]->loadWhenReady(argv[win.size()]);
	} else {
		newWindow();
	}
#else
	newWindow();
#endif

	return true;
}

IMPLEMENT_APP(Channel3);
