// Standard FileIO class, uses configuration window for configuration

#include "FileIO.h"
#include "PlotBox.h"

void FileIO::loadConfig(PlotBox* plot, int binaryLength, int flag) {
     plot->loadConfigFromWindow(flag);
     plot->verticalViewSize = (plot->displayMax - plot->displayMin);
     plot->lastAdcMin = plot->flagToFile(flag)->adcMin;
     plot->lastAdcMax = plot->flagToFile(flag)->adcMax;
     plot->setPlotMin(plot->displayMin);
}

int FileIO::read(PlotBox* plot, int binaryLength, int flag) {
	FileStruct *fs = plot->flagToFile(flag);
	plot->parseFileContents(flag);
	return fs->headerSize;
}

void FileIO::write(PlotBox* plot, wxString filename, bool append, std::vector<short>& data, int flag) {
	int continueWriting;
	continueWriting = wxMessageBox(_("The file will be written with the computer's native settings.\nThese are: Signed Data, LSB, 16 bit"),
			_("Headerless output"), wxOK|wxCANCEL);
	if(continueWriting != wxOK) return;
	wxString mode = _("wb");
	if (append) mode = _("ab");
	wxFFile out(filename, mode);
	if(!out.IsOpened()) return;

	short* actualBuffer = new short[data.size()];

	for(int i = 0; i < data.size(); i++)
		actualBuffer[i] = data.at(i);
	out.Write(actualBuffer, data.size() * sizeof(short));
	delete[] actualBuffer;

	out.Close();
}
