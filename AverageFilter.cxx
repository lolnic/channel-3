#include "AverageFilter.h"
#include "PlotBox.h"

AverageFilter::AverageFilter() : Filter(), range(1){      
}

wxString AverageFilter::paramName(){
        return _("Average Filter");
}

void AverageFilter::setParam(wxTextCtrl* ctrl) {
        ctrl->Enable();
        ctrl->SetValue(_(""));
        *ctrl << range;
}

bool AverageFilter::verifyInput(wxTextCtrl* ctrl){
        long tmp;
        if(!ctrl->GetValue().ToLong(&tmp)){
                wxMessageBox(_("Range not a valid integer"));
                return false;
        }
        if(tmp % 2 != 1 || tmp < 1){
                wxMessageBox(_("Range is not odd or is less than 1"));
                return false;
        }
        return true;
}

void AverageFilter::harvestInput(wxTextCtrl* ctrl){
        ctrl->GetValue().ToLong(&range);
}

void AverageFilter::apply(PlotBox* plot, int startpoint, int size){
        std::vector<short> averaged(plot->standardFile.dataPoints);
        int start = startpoint-range;
        int end = startpoint-1;
        int middle = (start+end)/2;
        int sum = 0;
        while(middle < startpoint + size){
                if(end >= startpoint && end < startpoint + size)
                        sum += plot->standardFile.dataPoints[end];
                if(start > startpoint)
                        sum -= plot->standardFile.dataPoints[start-1];
                if(middle >= startpoint)
                        averaged[middle] = sum / (std::min(end, (int)plot->standardFile.dataPoints.size()-1) - std::max(0, start) + 1);
                start++;
                end++;
                middle++;
        }
        plot->standardFile.dataPoints = averaged;
}
