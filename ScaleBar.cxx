#include "ScaleBar.h"
#include <algorithm>
using namespace std;

// Private methods
wxString ScaleBar::float2str(double number, int exp){
	if(exp > 0)exp = 0;
	exp = -exp;
	wxString result;
	result.Printf(_("%.*f"), exp, number);
	return result;
}

double ScaleBar::division(double size, int& exponent){
	double exp = floor(log10(size));
	double mantissa = size / pow(10, exp);
	exp--;
	if(mantissa > 7.5){
		exp++;
		exponent = exp;
		return 1 * pow(10, exp);
	}
	exponent = exp;
	if(mantissa > 3.5)
		return 5 * pow(10, exp);
	if(mantissa > 1.5)
		return 2 * pow(10, exp);
	return pow(10, exp);
}

void ScaleBar::dataPointToTriangle(int dataPoint, wxDC* dc, wxPoint* triangle) {
	int zerow, zeroh, width, height;
	dc->GetTextExtent(_("0"), &zerow, &zeroh);
	width = dc->GetSize().GetWidth();
	height = dc->GetSize().GetHeight()-20;
	int xOffset = ((float)plot->horizontalMarkers[dataPoint] - plot->getStartPoint()) / plot->viewSize * width - 6;
	
	triangle[0] = wxPoint(xOffset + 6, 0);
	triangle[1] = wxPoint(xOffset + 0, height - zeroh);
	triangle[2] = wxPoint(xOffset + 11, height - zeroh);
}

void ScaleBar::loadPreferences(){
	wxConfig *config = new wxConfig(_(APP_NAME));
	foregroundColour.Set(config->Read(_("/Colours/scaleForeground"), _("#000000")));
	backgroundColour.Set(config->Read(_("/Colours/scaleBackground"), _("#ffffff")));
	delete config;
}

void ScaleBar::savePreferences(){
	wxConfig *config = new wxConfig(_(APP_NAME));
	config->Write(_("/Colours/scaleForeground"), foregroundColour.GetAsString(wxC2S_HTML_SYNTAX));
	config->Write(_("/Colours/scaleBackground"), backgroundColour.GetAsString(wxC2S_HTML_SYNTAX));
	delete config;
}

// Overrided methods

ScaleBar::ScaleBar(wxPanel *parent, int id, wxPoint pos, wxSize size, long style) :
		   wxPanel(parent, id, pos, size, style), mode(SCALEBAR_HORIZONTAL), plot(NULL), moving(false), focussedMarker(-1)
{
	loadPreferences();
	backgroundBrush = new wxBrush(backgroundColour);
	foregroundPen = new wxPen(foregroundColour);
	markerBrush = new wxBrush(foregroundColour);
	markerPen = new wxPen(foregroundColour);
	std::vector<wxStaticText*> statics = ((MainWindow*)parent->GetParent())->staticTexts;
	for (int i = 0; i < statics.size(); i++) {
		statics[i]->SetForegroundColour(foregroundColour);
	}
	parent->SetForegroundColour(foregroundColour);
	parent->SetBackgroundColour(backgroundColour);
	
	Connect(wxEVT_PAINT, wxPaintEventHandler(ScaleBar::OnPaint));
	Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(ScaleBar::OnLeftClick));
	Connect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(ScaleBar::OnRightClick));
	Connect(wxEVT_LEFT_UP, wxMouseEventHandler(ScaleBar::OnLeftUp));
	Connect(wxEVT_MOTION, wxMouseEventHandler(ScaleBar::OnMouseMove));
}

ScaleBar::~ScaleBar(){
	savePreferences();
}

void ScaleBar::OnLeftClick(wxMouseEvent& event){
	if(mode != SCALEBAR_HORIZONTAL) return;
	
	int tabHit = -1;
	wxClientDC dc(this);
	int xAxisHit = event.GetLogicalPosition(dc).x;
	
	for (int i = 0; i < plot->horizontalMarkers.size(); i++) {
		wxPoint triangle[3];
		dataPointToTriangle(i, &dc, triangle);
		if (triangle[1].x < xAxisHit && xAxisHit < triangle[2].x) {
			tabHit = i;
		}
	}
	
	if (tabHit == -1) {
		int newMarkerPos = plot->getStartPoint() + xAxisHit * (long long)plot->viewSize / dc.GetSize().GetWidth();
		plot->horizontalMarkers.push_back(newMarkerPos);
		sort(plot->horizontalMarkers.begin(), plot->horizontalMarkers.end());
		for (int i = 0; i < plot->horizontalMarkers.size(); i++) {
			if (plot->horizontalMarkers[i] == newMarkerPos) focussedMarker = i;
		}
		Refresh();
		plot->Refresh();
		Update();
		plot->Update();
	} else {
		moving = true;
		movingMarker = tabHit;
		focussedMarker = tabHit;
	}
	Refresh();
	Update();
}

void ScaleBar::OnRightClick(wxMouseEvent& event) {
	if(mode != SCALEBAR_HORIZONTAL) return;
	
	int tabHit = -1;
	wxClientDC dc(this);
	int xAxisHit = event.GetLogicalPosition(dc).x;
	
	for (int i = 0; i < plot->horizontalMarkers.size(); i++) {
		wxPoint triangle[3];
		dataPointToTriangle(i, &dc, triangle);
		if (triangle[1].x < xAxisHit && xAxisHit < triangle[2].x) {
			tabHit = i;
		}
	}
	
	if(tabHit != -1) {
		for (int i = tabHit; i < plot->horizontalMarkers.size() - 1; i++) {
			plot->horizontalMarkers[i] = plot->horizontalMarkers[i+1];
		}
		if (tabHit < focussedMarker) focussedMarker--;
		else if (tabHit == focussedMarker) focussedMarker = -1;
		plot->horizontalMarkers.pop_back();
		Refresh();
		plot->Refresh();
		Update();
		plot->Update();
	}
}

void ScaleBar::fixMarkers() {
	// Adjust the horizontal markers so that they are still sorted
	while(movingMarker > 0 && plot->horizontalMarkers[movingMarker] < plot->horizontalMarkers[movingMarker-1]) {
		swap(plot->horizontalMarkers[movingMarker], plot->horizontalMarkers[movingMarker-1]);
		movingMarker--;
		focussedMarker--;
	}
	while(movingMarker < plot->horizontalMarkers.size()-1 && plot->horizontalMarkers[movingMarker] > plot->horizontalMarkers[movingMarker+1]) {
		swap(plot->horizontalMarkers[movingMarker], plot->horizontalMarkers[movingMarker+1]);
		movingMarker++;
		focussedMarker++;
	}
}

void ScaleBar::OnMouseMove(wxMouseEvent& event) {
	if(mode != SCALEBAR_HORIZONTAL) return;
	
	if(event.Dragging() && moving) {
		wxClientDC dc(this);
		int x = event.GetLogicalPosition(dc).x;
		if (x >= 0 && x < dc.GetSize().GetWidth()) {
			plot->horizontalMarkers[movingMarker] = plot->getStartPoint() + x * (long long)plot->viewSize / dc.GetSize().GetWidth();
		}
		
		fixMarkers();
		
		Refresh();
		plot->Refresh();
		Update();
		plot->Update();
	}
}

void ScaleBar::OnLeftUp(wxMouseEvent& event){
	if(mode != SCALEBAR_HORIZONTAL) return;
	
	moving = false;
}

void ScaleBar::OnPaint(wxPaintEvent& event){
	wxPaintDC dc(this);
	dc.SetBackground(*backgroundBrush);
	dc.SetTextForeground(foregroundColour);
	dc.SetPen(*foregroundPen);
	dc.Clear();
	
	if(plot == NULL || plot->file() == _(""))return;

	if(mode == SCALEBAR_HORIZONTAL)PaintHorizontal(dc);
	else PaintVertical(dc);
}

void ScaleBar::PaintHorizontal(wxPaintDC& dc){	
	
	// Get the size of the scale divisions
	double startTime, endTime, divisionTime, firstPlot;
	int exponent;
	startTime = plot->getStartPoint() * plot->standardFile.interval;
	endTime = (plot->getStartPoint() + plot->viewSize) * plot->standardFile.interval;
	divisionTime = division(endTime-startTime, exponent);

	// Get the time of the first scale marking
	firstPlot = ((int)(startTime/divisionTime)) * divisionTime;

	// Get the width and height of a zero
	int zerow, zeroh;
	dc.GetTextExtent(_("0"), &zerow, &zeroh);

	int minimumPlotStart = 0;
	long double scaleMarking = firstPlot;

	int width, height;
	width = dc.GetSize().GetWidth();
	height = dc.GetSize().GetHeight()-20;

	// Prepare the DC for drawing the markers
	wxColour oldForeground, oldBackground;
	oldForeground = GetForegroundColour();
	oldBackground = GetBackgroundColour();
	
	dc.SetBrush(*wxRED_BRUSH);
	dc.SetPen(*wxRED);
	
	// Plot the markers
	vector<int>::iterator firstMarker = lower_bound(plot->horizontalMarkers.begin(), plot->horizontalMarkers.end(), plot->getStartPoint());
	for (int i = firstMarker - plot->horizontalMarkers.begin();
		i < plot->horizontalMarkers.size() && plot->horizontalMarkers.at(i) < plot->getStartPoint() + plot->viewSize;
	     i++) {
			wxPoint triangle[3];
			dataPointToTriangle(i, &dc, triangle);
			if (focussedMarker == i) {
				dc.SetPen(*wxBLACK);
			} else {
				dc.SetPen(*wxRED);
			}
 			dc.DrawPolygon(3, triangle);
			
			int markerNumber = i + 1;
			wxString toDraw;
			toDraw.Printf(_("%d"), markerNumber);
			int w, h;
			dc.GetTextExtent(toDraw, &w, &h);
			
			dc.DrawText(toDraw, triangle[0].x - w/2, dc.GetSize().GetHeight() - h - 5);
	}
	
	// Prepare the DC for drawing the scale
	dc.SetPen(*foregroundPen);

	while(scaleMarking < endTime){
		int numberWidth = 0, numberHeight = 0;
		wxString plotString = float2str(scaleMarking/1000, exponent).c_str();
		dc.GetTextExtent(plotString, &numberWidth, &numberHeight);
		int plotAtX;
		plotAtX = (scaleMarking / plot->standardFile.interval - plot->getStartPoint()) / (double)plot->viewSize * width - numberWidth/2;
		dc.DrawLine(plotAtX+numberWidth/2, 0, plotAtX+numberWidth/2, height/2);
		if(plotAtX >= minimumPlotStart + zerow){
			dc.DrawText(plotString, plotAtX, height - numberHeight);
			minimumPlotStart = plotAtX + numberWidth;
		}
		int pointsPlotted = 0;
		for(	double i = plotAtX;
				i < plotAtX + divisionTime / plot->standardFile.interval / plot->viewSize * width;
				i += divisionTime / plot->standardFile.interval / plot->viewSize * width / 10){
			dc.DrawLine(i + numberWidth/2, 0, i + numberWidth/2, height/4);
			if(pointsPlotted >= 9)break;
			pointsPlotted++;
		}
		scaleMarking += divisionTime;
	}
}

void ScaleBar::PaintVertical(wxPaintDC& dc){
	double minimum, maximum, totalsize, divisionsize, firstPlot, scaleMarking;
	int exponent;
	minimum = plot->getPlotMin();
	maximum = plot->getPlotMin() + plot->getPlotRange();
	if(minimum == maximum){
	    return;
	}
	totalsize = maximum-minimum;
	divisionsize = division(totalsize, exponent);
	firstPlot = ((int)(minimum / divisionsize))*divisionsize;
	scaleMarking = firstPlot;

	while(scaleMarking < maximum){
		wxString plotString = float2str(scaleMarking, exponent);
		int textHeight, textWidth;
		dc.GetTextExtent(plotString, &textWidth, &textHeight);
		int plotAtY = dc.GetSize().GetHeight() - ((scaleMarking - minimum) / totalsize * dc.GetSize().GetHeight());
		dc.DrawLine(dc.GetSize().GetWidth(), plotAtY, dc.GetSize().GetWidth()/8*5, plotAtY);
		dc.DrawText(plotString, dc.GetSize().GetWidth()/8*5 - textWidth - 3, plotAtY-textHeight/2);
		for(double i = scaleMarking + divisionsize/5; i < scaleMarking + divisionsize; i += divisionsize/5) {
			double subPlotAtY = dc.GetSize().GetHeight() - ((i - minimum) / totalsize * dc.GetSize().GetHeight());
			dc.DrawLine(dc.GetSize().GetWidth(), subPlotAtY, dc.GetSize().GetWidth()/8*7, subPlotAtY);
		}
		scaleMarking += divisionsize;
	}	
}

// Public Methods

void ScaleBar::shiftMarker(int direction) {
	assert(mode == SCALEBAR_HORIZONTAL);
	assert(direction == 1 || direction == -1);
	if (focussedMarker != -1) {
		plot->horizontalMarkers[focussedMarker] += direction;
		fixMarkers();
		Refresh();
		Update();
		plot->Refresh();
		plot->Update();
	}
}

void ScaleBar::clearMarkers() {
	plot->horizontalMarkers.clear();
	focussedMarker = -1;
	moving = false;
}

void ScaleBar::setmode(int m){
	if(m == SCALEBAR_VERTICAL || m == SCALEBAR_HORIZONTAL)
		mode = m;
}

void ScaleBar::associatePlotBox(PlotBox *p){
	plot = p;
}

bool ScaleBar::SetForegroundColour(const wxColour& colour){
	if(!colour.IsOk())return false;
	foregroundColour = colour;
	foregroundPen->SetColour(foregroundColour);
	return true;
}

wxColour ScaleBar::GetForegroundColour(){
	return foregroundColour;
}

bool ScaleBar::SetBackgroundColour(const wxColour& colour){
	if(!colour.IsOk())return false;
	backgroundColour = colour;
	backgroundBrush->SetColour(backgroundColour);
	return true;
}

wxColour ScaleBar::GetBackgroundColour(){
	return backgroundColour;
}
