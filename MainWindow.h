#ifndef _MAINWINDOW_
#define _MAINWINDOW_

#define TXT_FILES 1
#define DAT_FILES (1<<1)
#define PRN_FILES (1<<2)
#define ALL_FILES (1<<3)
#define APPEND 1
#define OVERWRITE 2
class MainWindow;

#include <wx/spinctrl.h>
#include "ConfigurationWindow.h"
#include "PrintDialog.h"
#include "Histogram.h"
#include "ReduceDialog.h"
#include "PreferencesDialog.h"
#include "PlotBox.h"
#include "PlotPanel.h"
#include "ScaleBar.h"
#include "TabBar.h"
#include "channel3.h"
#include "config.h"

class MainWindow : public wxFrame{
	public:
		MainWindow(const wxString& title, const wxPoint& pos, const wxSize& size);
		void adjustZoomControls();
		void loadWhenReady(wxString file);

		bool fileWaiting;
		wxString waitingFile;
		bool ignoreNextMarkerEvent;
		wxSlider *horizontalZoom;
		TabBar *tabBar;
		PlotPanel* plotPanel;
		wxTextCtrl *dropoutInput, *gotoInput;
		wxSpinCtrl* markerInput;
		wxTimer *updateTimer;
		wxStaticText* viewSizeDisplay;
		wxStaticText* openDisplay;
		wxStaticText* closedDisplay;
		wxStaticText* baseDisplay;
		wxStaticText* diffDisplay;
		wxStaticText* markerPosDisplay;
		PrintDialog *printWindow;
		Histogram *histWindow;
		ReduceDialog *reduceWindow;
		std::vector<wxStaticText*> staticTexts;
		int lastGotoVal;

		wxPanel *panel;

		Filter* currentFilter;
		int nFilters;
		Filter ** filters;
		wxTextCtrl *filterParamInput;
		wxStaticText *filterParamName;

		// menu callbacks
		void OnQuit(wxCommandEvent& event);
		void OnOpenFile(wxCommandEvent& event);
		void OnOpenAltFile(wxCommandEvent& event);
		void OnSaveAs(wxCommandEvent& event);
		void OnSaveOddEven(wxCommandEvent& event);
		void OnSaveEvenOdd(wxCommandEvent& event);
		void OnSaveFirstMarks(wxCommandEvent& event);
		void OnFilters(wxCommandEvent& event);
		void OnReduces(wxCommandEvent& event);
		void OnConfigurationWindow(wxCommandEvent& event);
		void OnToggleFullScreen(wxCommandEvent& event);
		void OnPreferences(wxCommandEvent& event);
		void OnAbout(wxCommandEvent& event);
		void OnAnalyse(wxCommandEvent& event);
		void OnNewWindow(wxCommandEvent& event);
		void OnToggleInvert(wxCommandEvent& event);
		void OnPrint(wxCommandEvent& event);
		void OnHistogram(wxCommandEvent& event);
		void OnNoFilter(wxCommandEvent& event);
		void OnAverageFilter(wxCommandEvent& event);
		void OnMedianFilter(wxCommandEvent& event);
		void OnGaussionFilter(wxCommandEvent& event);

		// Zoom callbacks
		void OnZoomHorizontal(wxScrollEvent& event);

		//Button Callbacks
		void OnClearMarkers(wxCommandEvent& event);

		// Text entry callbacks
		void OnDropoutChange(wxCommandEvent& event);
		void OnGotoChange(wxCommandEvent& event);
		void OnFilterParamChange(wxCommandEvent& event);

		void markerChange();
		void OnMarkerSpin(wxSpinEvent& event);
		void OnMarkerChange(wxCommandEvent& event);

		// Timer callbacks
		void updateInterval(wxTimerEvent& event);

		void OnClose(wxCloseEvent& event);

		void ChangeFilter(Filter* toApply);
		void updateGotoBox ();
		void updateViewSizeDisplay ();
		int fileDialog (int fileType, wxString& filePath, long style = wxFD_SAVE);

		DECLARE_EVENT_TABLE()
};

#endif
