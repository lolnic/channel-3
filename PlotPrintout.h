#ifndef _PLOTPRINT_
#define _PLOTPRINT_

#include <wx/wx.h>
#include <wx/print.h>
#include "PlotBox.h"

class PlotPrintout : public wxPrintout {
	public:
	PlotPrintout(wxString& fname, PlotBox*, int ppl, int lpp, int start, int end, int dropout, wxString comment, wxString title);

	float m_units_per_cm;
	wxPageSetupDialogData m_page_setup;
	wxPaperSize m_paper_type;
	int m_orient;
	int m_margin_left, m_margin_right, m_margin_top, m_margin_bottom;
	int m_coord_system_width, m_coord_system_height;

	int pointsPerLine, linesPerPage, start, end, dropout;
	int nPages, nLines;
	int infoX;
	wxFont *printFont;
	wxCoord width, height;
	wxString comment, title, fname;

	bool performPageSetup(const bool showPageSetupDialog);
	wxPrintData getPrintData();
	void copyPageSetup(PlotPrintout& other);

	void GetPageInfo(int *minPage, int *maxPage, int *pageFrom, int *pageTo);
	bool OnBeginDocument(int startPage, int endPage);
	void OnBeginPrinting();
	void OnPreparePrinting();
	bool HasPage(int pageNum);
	bool OnPrintPage(int pageNum);
	void printInfo(wxDC *dc, wxString data);
	wxCoord scale(wxCoord width, short smallest, short biggest, short data);

	PlotBox *plot;
};

#endif
