#ifndef _HISTOGRAM_
#define _HISTOGRAM_

class Histogram;

#include "channel3.h"
#include "PlotBox.h"
#include "HistBox.h"
#include <wx/filepicker.h>

class Histogram : public wxFrame{
	public:
		Histogram(MainWindow *parent, PlotBox* plot);
		MainWindow *win;
	private:
		PlotBox *plot;
		HistBox *hist;
		wxPanel *panel;
		MainWindow *parent;

		wxFilePickerCtrl * fileNameInput;
		wxTextCtrl *scaleMinInput, *scaleMaxInput, *frequencyMinInput, *frequencyMaxInput;
		wxSpinCtrl *startMarkerInput, *endMarkerInput, *binWidthInput;
		wxCheckBox *wholeFileCheck;

		double scaleMin, scaleMax, frequencyMin, frequencyMax;
		int startMarker, endMarker, binWidth;
		int firstNonZero, lastNonZero;

		void SetDefaults();
		bool harvestAllInput();
		bool harvestDouble(wxTextCtrl*, double*);

		void OnPrint(wxCommandEvent& event);
		void OnCancel(wxCommandEvent& event);
		void OnPreview(wxCommandEvent& event);
		void OnWholeFileCheck(wxCommandEvent& event);
		void OnClose(wxCloseEvent& event);

		DECLARE_EVENT_TABLE();
};

#endif
