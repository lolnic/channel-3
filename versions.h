#ifndef __VERSIONS__
#define __VERSIONS__

#define NVERSIONS 1
#define VERSIONSTRINGSIZE 42

extern char versions[NVERSIONS][VERSIONSTRINGSIZE];

int versionNumber(unsigned char *file, int filesize);

#endif

