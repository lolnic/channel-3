#include <cassert>
#include <cstring>
#include "versions.h"

char versions[NVERSIONS][VERSIONSTRINGSIZE] = {
	"This is a Channel 3 file. File version 2."
};

bool isVersion(int number, unsigned char *file){
	assert(0 <= number && number < NVERSIONS);
	
	for(int i = 0; i < strlen(versions[number]); i++)
		if(versions[number][i] != file[i])return false;
	return true;
}

int versionNumber(unsigned char *file, int length){
	assert(length >= 0);
	
	if(length < VERSIONSTRINGSIZE)return 0;
	for(int i = 0; i < NVERSIONS; i++)
		if(isVersion(i, file))return i + 1;
	return 0;
}
