#include "Histogram.h"

enum {
	ID_Filename=1,
	ID_StartMarker,
	ID_EndMarker,
	ID_WholeFile,
	ID_ScaleMin,
	ID_ScaleMax,
	ID_FrequencyMin,
	ID_FrequencyMax,
	ID_BinWidth
};

BEGIN_EVENT_TABLE(Histogram, wxFrame)
	EVT_BUTTON(wxID_OK, Histogram::OnPrint)
	EVT_BUTTON(wxID_APPLY, Histogram::OnPreview)
	EVT_BUTTON(wxID_CANCEL, Histogram::OnCancel)
	EVT_CHECKBOX(ID_WholeFile, Histogram::OnWholeFileCheck)
	EVT_CLOSE(Histogram::OnClose)
END_EVENT_TABLE()

Histogram::Histogram(MainWindow *parent, PlotBox *plot)
	: wxFrame(parent, -1, _("Amplitude Histogram"), wxDefaultPosition, wxDefaultSize,
			 wxFULL_REPAINT_ON_RESIZE | wxDEFAULT_FRAME_STYLE),
	plot(plot), frequencyMin(0)
{
	panel = new wxPanel(this, wxID_ANY);
	this->parent = parent;
	wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *bottomSizer = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer * prefSizer = new wxBoxSizer(wxVERTICAL);

	sizer->SetSizeHints(panel);
	panel->SetSizer(sizer);


	hist = new HistBox(panel, wxID_ANY, wxDefaultPosition, wxSize(800, 800));
	hist->associatePlotBox(plot);
	//prefSizer->Add(new wxStaticText(panel, wxID_ANY, _("Filename")), 0, wxTOP|wxRIGHT, 5);
	//prefSizer->Add(fileNameInput = new wxFilePickerCtrl(panel, ID_Filename, _(""), _("Select a file"), _("Text Files (*.txt)|*.txt"),
	//	wxDefaultPosition, wxDefaultSize, wxFLP_SAVE|wxFLP_OVERWRITE_PROMPT|wxFLP_USE_TEXTCTRL), 0, wxTOP|wxRIGHT|wxSHAPED, 5);
	prefSizer->Add(new wxStaticText(panel, wxID_ANY, _("Start Marker")), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(startMarkerInput = new wxSpinCtrl(panel, ID_StartMarker), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(new wxStaticText(panel, wxID_ANY, _("End Marker")), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(endMarkerInput = new wxSpinCtrl(panel, ID_EndMarker), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(wholeFileCheck = new wxCheckBox(panel, ID_WholeFile, _("Whole File")), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(new wxStaticText(panel, wxID_ANY, _("Scale Min")), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(scaleMinInput = new wxTextCtrl(panel, ID_ScaleMin), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(new wxStaticText(panel, wxID_ANY, _("Scale Max")), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(scaleMaxInput = new wxTextCtrl(panel, ID_ScaleMax), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(new wxStaticText(panel, wxID_ANY, _("Frequency Max")), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(frequencyMaxInput = new wxTextCtrl(panel, ID_FrequencyMax), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(new wxStaticText(panel, wxID_ANY, _("Bin Width")), 0, wxTOP|wxRIGHT, 5);
	prefSizer->Add(binWidthInput = new wxSpinCtrl(panel, ID_BinWidth), 0, wxTOP|wxRIGHT, 5);
	SetDefaults();
	bottomSizer->Add(hist, 1, wxEXPAND);
	bottomSizer->Add(prefSizer, 0, wxLEFT, 5);
	sizer->Add(bottomSizer, 1, wxEXPAND);

	wxStdDialogButtonSizer *buttonSizer = new wxStdDialogButtonSizer();
	buttonSizer->AddButton(new wxButton(panel, wxID_OK, _("Save")));
	buttonSizer->AddButton(new wxButton(panel, wxID_APPLY, _("Generate")));
	buttonSizer->AddButton(new wxButton(panel, wxID_CANCEL, _("Cancel")));
	buttonSizer->Realize();
	sizer->Add(buttonSizer, 0, wxSHAPED);

	panel->Fit();
	Fit();
	Centre();
}

void Histogram::SetDefaults() {
	wxString name;
	wxFileName tmp;
	tmp.SplitPath(plot->file(), NULL, NULL, &name, NULL);
	SetTitle(name + _(" - Histogram - ") + _(FULL_APP_NAME));

	*scaleMinInput << plot->displayMin;
	*scaleMaxInput << plot->displayMax;

	*frequencyMaxInput << _("auto");

	startMarkerInput->SetRange(0, plot->horizontalMarkers.size());
	startMarkerInput->SetValue(0);
	endMarkerInput->SetRange(1, plot->horizontalMarkers.size()+1);
	endMarkerInput->SetValue(plot->horizontalMarkers.size()+1);

	binWidthInput->SetRange(1, 10000);
	binWidthInput->SetValue(1);
}

bool Histogram::harvestDouble(wxTextCtrl* ctrl, double* dbl) {
	if (ctrl == frequencyMaxInput && ctrl->GetValue() == _("auto")) {
		*dbl = -1;
		return true;
	}
	return ctrl->GetValue().ToDouble(dbl);
}

bool Histogram::harvestAllInput() {
	startMarker = startMarkerInput->GetValue();
	endMarker = endMarkerInput->GetValue();
	binWidth = binWidthInput->GetValue();
	if (endMarker <= startMarker) {
		wxMessageBox(_("Start marker must precede end marker"));
		return false;
	}
	if (!(harvestDouble(scaleMinInput, &scaleMin) && harvestDouble(scaleMaxInput, &scaleMax)
		 && harvestDouble(frequencyMaxInput, &frequencyMax))) {
		wxMessageBox(_("Invalid floating point value"));
		return false;
	}
	if (scaleMax <= scaleMin) {
		wxMessageBox(_("Scale min must be less than scale max"));
		return false;
	}
	return true;
}

void Histogram::OnWholeFileCheck(wxCommandEvent& event) {
	if (wholeFileCheck->GetValue()) {
		startMarkerInput->SetValue(0);
		endMarkerInput->SetValue(plot->horizontalMarkers.size()+1);
		startMarkerInput->Disable();
		endMarkerInput->Disable();
	} else {
		startMarkerInput->Enable();
		endMarkerInput->Enable();
	}
}

void Histogram::OnPrint(wxCommandEvent& event) {
	this->Disable();
	wxString filename;
	int mode = win->fileDialog(TXT_FILES, filename, wxFD_SAVE);
	wxString fmode = _("wb");
	if (mode == APPEND) fmode = _("ab");
	if (mode) hist->writeToFile(filename, fmode);
	wxMilliSleep(500);
	this->Enable();
	return;
}

void Histogram::OnPreview(wxCommandEvent& event) {
	if (harvestAllInput()) {
		int start, end;
		if (startMarker == 0) start = 0;
		else start = plot->horizontalMarkers[startMarker-1];
		if (endMarker == plot->horizontalMarkers.size()+1) end = plot->standardFile.dataPoints.size();
		else end = plot->horizontalMarkers[endMarker-1];
		hist->generate(scaleMin, scaleMax, frequencyMin, frequencyMax, start, end, binWidth);
		hist->Refresh();
		hist->Update();
	}
}

void Histogram::OnCancel(wxCommandEvent& event) {
	parent->histWindow = NULL;
	this->Destroy();
}

void Histogram::OnClose(wxCloseEvent& event) {
	parent->histWindow = NULL;
	this->Destroy();
}
