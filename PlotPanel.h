#ifndef _PLOTPANEL_
#define _PLOTPANEL_

#define PLOT_MARGIN 70
#define ZOOM_BUTTON_SIZE 30
#define SCROLLBAR_HEIGHT 20
#define SCALEBAR_HEIGHT 50

class PlotPanel;

#include "channel3.h"
#include "PlotBox.h"
#include "ScaleBar.h"
#include <wx/wx.h>

class PlotPanel : public wxPanel {
public:
  PlotBox* plot;
  ScaleBar *horizontalScale;
  ScaleBar *verticalScale;
  wxSlider *verticalZoom;
  wxScrollBar* verticalScrollbar;
  wxScrollBar* fileScrollbar;

  PlotPanel(
    wxWindow* parent,
    wxWindowID id=wxID_ANY,
    wxScrollBar* fileScrollbar=NULL,
    const wxPoint &pos=wxDefaultPosition,
    const wxSize &size=wxDefaultSize,
    long style=wxTAB_TRAVERSAL,
    const wxString &name=wxPanelNameStr
  );
  ~PlotPanel();

  void adjustZoomControls();
  void clearMarkers();
  int getFocussedMarker();
  void removeFilters();
  PlotBox* getPlot();

  void OnVScrollMove(wxScrollEvent& event);
  void OnScrollbarMove(wxScrollEvent& event);
  void OnZoomVertical(wxScrollEvent& event);

  wxDECLARE_EVENT_TABLE();
};

#endif
