#ifndef _AVERAGE_FILTER_
#define _AVERAGE_FILTER_

#include "Filter.h"
#include <algorithm>

class AverageFilter : public Filter{
        long range;
public:
        AverageFilter();
        wxString paramName();
        void setParam(wxTextCtrl* ctrl);
        bool verifyInput(wxTextCtrl* ctrl);
        void harvestInput(wxTextCtrl* ctrl);
        void apply(PlotBox* plot, int start, int size);
};

#endif
