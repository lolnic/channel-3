#ifndef _TABBAR_
#define _TABBAR_
class TabBar;

#include "channel3.h"
#include "PlotBox.h"

class TabBar : public wxPanel {
     // Private methods
     void markerDimensions(int marker, int& x, int& y, int& w, int& h, wxDC* dc);
	void loadPreferences();
     
     //  Private variables
     PlotBox* plot;
     bool moving, movingAll;
     int movingMarker, startLoc, focussedMarker;
     
     wxColour foregroundColour, backgroundColour;
     wxBrush *backgroundBrush;
     wxPen *foregroundPen;
     
     wxBrush *markerBrush;
     wxPen *markerPen;
public:
     // Public methods
     TabBar(wxPanel *parent, int id, 
            wxPoint pos = wxDefaultPosition,
            wxSize size = wxDefaultSize, long style=0);
     void OnPaint(wxPaintEvent& event);
     void OnLeftClick(wxMouseEvent& event);
     void OnRightClick(wxMouseEvent& event);
     void OnMouseMove(wxMouseEvent& event);
     void OnLeftUp(wxMouseEvent& event);
	void OnRightUp(wxMouseEvent& event);
     
     void associatePlotBox(PlotBox *p);
     bool SetForegroundColour(const wxColour& colour);
     wxColour GetForegroundColour();
     bool SetBackgroundColour(const wxColour& colour);
     wxColour GetBackgroundColour();
     void shiftMarker(int amount);
};

#endif
