#ifndef _SCALEBAR_
#define _SCALEBAR_
#define SCALEBAR_VERTICAL 0
#define SCALEBAR_HORIZONTAL 1
class ScaleBar;

#include <sstream>
#include <cmath>
#include "channel3.h"
#include "MainWindow.h"
#include "PlotBox.h"

class ScaleBar : public wxPanel{
	private:
		// Private methods
		wxString float2str(double number, int exp);
		double division(double size, int& exponent);
		void dataPointToTriangle(int dataPoint, wxDC* dc, wxPoint* triangle);
		void loadPreferences();
		void fixMarkers();

		// Private variables
		int mode;
		PlotBox *plot;
		bool controlsPreferences;
		wxColour foregroundColour, backgroundColour;
		wxBrush *backgroundBrush;
		wxPen *foregroundPen;
		
		wxBrush *markerBrush;
		wxPen *markerPen;
		
		bool moving;
		int movingMarker;

	public:
		// Overrided methods
		ScaleBar(wxPanel *parent, int id, 
				wxPoint pos = wxDefaultPosition,
				wxSize size = wxDefaultSize, long style=0);
		~ScaleBar();

		// Event handlers
		void OnLeftClick(wxMouseEvent& event);
		void OnRightClick(wxMouseEvent& event);
		void OnMouseMove(wxMouseEvent& event);
		void OnLeftUp(wxMouseEvent& event);
		void OnPaint(wxPaintEvent& event);
		void PaintHorizontal(wxPaintDC& dc);
		void PaintVertical(wxPaintDC& dc);

		// Public variables
		int accuracy;
		int focussedMarker;

		// Public methods
		void savePreferences();
		void setmode(int m);
		void associatePlotBox(PlotBox *p);
		bool SetForegroundColour(const wxColour& colour);
		wxColour GetForegroundColour();
		bool SetBackgroundColour(const wxColour& colour);
		wxColour GetBackgroundColour();
		void shiftMarker(int direction);
		void clearMarkers();
};

#endif
