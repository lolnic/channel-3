#include "PreferencesDialog.h"

BEGIN_EVENT_TABLE(PreferencesDialog, wxFrame)
	EVT_BUTTON(wxID_OK, PreferencesDialog::OnOK)
	EVT_BUTTON(wxID_CANCEL, PreferencesDialog::OnCancel)
	EVT_BUTTON(wxID_NO, PreferencesDialog::OnDefault)
END_EVENT_TABLE();

void PreferencesDialog::OnOK(wxCommandEvent& event){
	plotPanel->plot->plotAltColour = plotAltPicker->GetColour();
	plotPanel->plot->plotForegroundColour = plotForegroundPicker->GetColour();
	plotPanel->plot->plotBackgroundColour = plotBackgroundPicker->GetColour();
	plotPanel->plot->markerColour = markerPicker->GetColour();
	plotPanel->plot->plotWithLines = plotLinesCheck->GetValue();
	plotPanel->plot->savePreferences();
	plotPanel->fileScrollbar->SetBackgroundColour(scaleBackgroundPicker->GetColour());
	plotPanel->verticalScrollbar->SetBackgroundColour(scaleBackgroundPicker->GetColour());
	plotPanel->verticalScale->SetForegroundColour(scaleForegroundPicker->GetColour());
	plotPanel->horizontalScale->SetForegroundColour(scaleForegroundPicker->GetColour());
	plotPanel->verticalScale->SetBackgroundColour(scaleBackgroundPicker->GetColour());
	plotPanel->horizontalScale->SetBackgroundColour(scaleBackgroundPicker->GetColour());
	plotPanel->verticalScale->savePreferences();
    tab1->SetBackgroundColour(scaleBackgroundPicker->GetColour());
    tab1->SetForegroundColour(scaleForegroundPicker->GetColour());
	m_parent->SetBackgroundColour(scaleBackgroundPicker->GetColour());
	m_parent->SetForegroundColour(scaleForegroundPicker->GetColour());
	for(int i = 0; i < statics.size(); i++) {
		statics[i]->SetForegroundColour(scaleForegroundPicker->GetColour());
	}

	GetParent()->Refresh();
	GetParent()->Update();
	Close();
	slide1->SetFocus();
}

void PreferencesDialog::OnDefault(wxCommandEvent& event) {
	plotForegroundPicker->SetColour(_("#000000"));
	plotBackgroundPicker->SetColour(_("#ffffff"));
	scaleForegroundPicker->SetColour(_("#000000"));
	scaleBackgroundPicker->SetColour(_("#ffffff"));
	markerPicker->SetColour(_("#000000"));
	plotAltPicker->SetColour(_("#AAAAAA"));
	plotLinesCheck->SetValue(true);
}

void PreferencesDialog::OnCancel(wxCommandEvent& event){
	Close();
}

PreferencesDialog::PreferencesDialog(wxWindow *parent, PlotPanel* plotPanel, TabBar* tab1, std::vector<wxStaticText*> statics, wxSlider* slide1)
		 : wxFrame(parent, -1, _("Filters"), wxDefaultPosition, wxDefaultSize,
		    wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxSYSTEM_MENU | wxCAPTION | wxCLIP_CHILDREN),
		    plotPanel(plotPanel), tab1(tab1), slide1(slide1)
{
	m_parent = parent;
	this->statics = statics;
	wxPanel *panel = new wxPanel(this, wxID_ANY);
	// Declare sizers
	wxFlexGridSizer *preferencesSizer = new wxFlexGridSizer(4, 5, 10);
	wxStdDialogButtonSizer *buttonSizer = new wxStdDialogButtonSizer();
	wxBoxSizer *mainSizer = new wxBoxSizer(wxVERTICAL);

	// Set up sizers
	panel->SetSizer(mainSizer);
	mainSizer->SetSizeHints(panel);
	mainSizer->Add(preferencesSizer, 1, wxCENTER|wxTOP|wxLEFT|wxRIGHT, 5);
	mainSizer->Add(buttonSizer, 0, wxALIGN_RIGHT|wxTOP, 5);

	preferencesSizer->Add(new wxStaticText(panel, wxID_ANY, _("Plot Foreground Colour")), 1, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
	preferencesSizer->Add(plotForegroundPicker = new wxColourPickerCtrl(panel, wxID_ANY, plotPanel->plot->plotForegroundColour));
	preferencesSizer->Add(new wxStaticText(panel, wxID_ANY, _("Plot Background Colour")), 1, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
	preferencesSizer->Add(plotBackgroundPicker = new wxColourPickerCtrl(panel, wxID_ANY, plotPanel->plot->plotBackgroundColour));
	preferencesSizer->Add(new wxStaticText(panel, wxID_ANY, _("Scale Foreground Colour")), 1, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
	preferencesSizer->Add(scaleForegroundPicker = new wxColourPickerCtrl(panel, wxID_ANY, plotPanel->horizontalScale->GetForegroundColour()));
	preferencesSizer->Add(new wxStaticText(panel, wxID_ANY, _("Scale Background Colour")), 1, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
	preferencesSizer->Add(scaleBackgroundPicker = new wxColourPickerCtrl(panel, wxID_ANY, plotPanel->horizontalScale->GetBackgroundColour()));
	preferencesSizer->Add(new wxStaticText(panel, wxID_ANY, _("Marker Colour")), 1, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
	preferencesSizer->Add(markerPicker = new wxColourPickerCtrl(panel, wxID_ANY, plotPanel->plot->markerColour));
	preferencesSizer->Add(new wxStaticText(panel, wxID_ANY, _("Plot Comparison Colour")), 1, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
	preferencesSizer->Add(plotAltPicker = new wxColourPickerCtrl(panel, wxID_ANY, plotPanel->plot->plotAltColour));
	preferencesSizer->AddStretchSpacer();
	preferencesSizer->Add(plotLinesCheck = new wxCheckBox(panel, wxID_ANY, _("Plot Lines")), 1, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL);
	plotLinesCheck->SetValue(plotPanel->plot->plotWithLines);

	buttonSizer->AddButton(defaultButton = new wxButton(panel, wxID_NO, _("Reset")));
	buttonSizer->AddButton(new wxButton(panel, wxID_OK));
	buttonSizer->AddButton(new wxButton(panel, wxID_CANCEL));
	buttonSizer->Realize();

	panel->Fit();
	Fit();
	Center();
	Show(true);
}
