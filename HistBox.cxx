#include "HistBox.h"
#include <climits>
#define LEFT_MARGIN 100
#define BOTTOM_MARGIN 100

HistBox::HistBox(wxPanel *parent, int id, wxPoint pos, wxSize size, long style) :
		   wxPanel(parent, id, pos, size, style), plot(NULL), freq(NULL)
{
	Connect(wxEVT_PAINT, wxPaintEventHandler(HistBox::OnPaint));
	firstBucket = lastBucket = 0;
}

void HistBox::writeToFile(wxString filename, wxString fmode) {
	if (firstBucket == lastBucket) {
		wxMessageBox("No histogram has been generated");
		return;
	}
	wxFFile out(filename, fmode);
	out.Write("Lower Bound\tUpper Bound\tFrequency\r\n");
	for (int i = firstBucket; i < lastBucket; i++) {
		float start = plot->toVoltage(i*binWidth+SHRT_MIN);
		float end = plot->toVoltage((i+1)*binWidth+SHRT_MIN);
		wxString line;
		line.Printf(_("%f\t%f\t%f\r\n"), start, end, freq[i]/(float)totalFrequency);
		out.Write(line);
	}
	out.Close();
}

wxString HistBox::float2str(double number, int exp){
	if(exp > 0)exp = 0;
	exp = -exp;
	wxString result;
	result.Printf(_("%.*f"), exp, number);
	return result;
}

double HistBox::division(double size, int& exponent){
	double exp = floor(log10(size));
	double mantissa = size / pow(10, exp);
	exp--;
	if(mantissa > 7.5){
		exp++;
		exponent = exp;
		return 1 * pow(10, exp);
	}
	exponent = exp;
	if(mantissa > 3.5)
		return 5 * pow(10, exp);
	if(mantissa > 1.5)
		return 2 * pow(10, exp);
	return pow(10, exp);
}

void HistBox::PaintVertical(wxPaintDC& dc){
	double minimum, maximum, totalsize, divisionsize, firstPlot, scaleMarking;
	int exponent;
	minimum = frequencyMin;
	maximum = frequencyMax;
	totalsize = maximum-minimum;
	divisionsize = division(totalsize, exponent);
	firstPlot = ((int)(minimum / divisionsize))*divisionsize;
	scaleMarking = firstPlot;

	int height = dc.GetSize().GetHeight()-BOTTOM_MARGIN;
	int width = LEFT_MARGIN;

	while(scaleMarking < maximum){
		wxString plotString = float2str(scaleMarking, exponent);
		int textHeight, textWidth;
		dc.GetTextExtent(plotString, &textWidth, &textHeight);
		int plotAtY = height - ((scaleMarking - minimum) / totalsize * height);
		dc.DrawLine(width, plotAtY, width/8*5, plotAtY);
		dc.DrawText(plotString, width/8*5 - textWidth - 3, plotAtY-textHeight/2);
		for(double i = scaleMarking + divisionsize/5; i < scaleMarking + divisionsize; i += divisionsize/5) {
			double subPlotAtY = height - ((i - minimum) / totalsize * height);
			dc.DrawLine(width, subPlotAtY, width/8*7, subPlotAtY);
		}
		scaleMarking += divisionsize;
	}	
}

void HistBox::PaintHorizontal(wxPaintDC& dc){	
	
	// Get the size of the scale divisions
	double startTime, endTime, divisionTime, firstPlot;
	int exponent;
	startTime = plot->toVoltage(firstBucket*binWidth+SHRT_MIN);
	endTime = plot->toVoltage(lastBucket*binWidth+SHRT_MIN);
	divisionTime = division(endTime-startTime, exponent);

	// Get the time of the first scale marking
	firstPlot = ((int)(startTime/divisionTime)) * divisionTime;

	// Get the width and height of a zero
	int zerow, zeroh;
	dc.GetTextExtent(_("0"), &zerow, &zeroh);

	int minimumPlotStart = 0;
	long double scaleMarking = firstPlot;

	int width, height;
	width = dc.GetSize().GetWidth();
	height = dc.GetSize().GetHeight();
	int topMargin = height-BOTTOM_MARGIN;

	while(scaleMarking < endTime){
		int numberWidth = 0, numberHeight = 0;
		wxString plotString = float2str(scaleMarking, exponent).c_str();
		dc.GetTextExtent(plotString, &numberWidth, &numberHeight);
		int plotAtX;
		plotAtX = LEFT_MARGIN + (scaleMarking-startTime)/(endTime-startTime)*(width-LEFT_MARGIN);
		dc.DrawLine(plotAtX, topMargin, plotAtX, topMargin+BOTTOM_MARGIN/2);
		if(plotAtX >= minimumPlotStart + zerow){
			dc.DrawText(plotString, plotAtX-numberWidth/2, height - numberHeight);
			minimumPlotStart = plotAtX + numberWidth;
		}
		int pointsPlotted = 0;
		scaleMarking += divisionTime;
		int nextPlotAtX = LEFT_MARGIN + (scaleMarking-startTime)/(endTime-startTime)*(width-LEFT_MARGIN);
		for (int i = 1; i <= 9; i++) {
			int p = plotAtX + (nextPlotAtX-plotAtX)*(i/10.0);
			dc.DrawLine(p, topMargin, p, topMargin+BOTTOM_MARGIN/4);
		}
	}
}

void HistBox::OnPaint(wxPaintEvent& event){
	wxPaintDC dc(this);
	dc.SetBackground(*wxWHITE_BRUSH);
	dc.Clear();
	int graphWidth = dc.GetSize().GetWidth() - LEFT_MARGIN;
	int graphHeight = dc.GetSize().GetHeight() - BOTTOM_MARGIN;

	int exponent;
	division(scaleMax-scaleMin, exponent);
	if (firstBucket == lastBucket) return;
	int range = (lastBucket-firstBucket);
	for (int i = std::max(firstNonZero, firstBucket); i <= std::min(lastNonZero, lastBucket-1); i++) {
		int x = LEFT_MARGIN+(i-firstBucket)*(graphWidth/(double)range);
		int rectWidth = (graphWidth-x+LEFT_MARGIN) / (lastBucket-i);
		int dp = freq[i];
		int rectHeight = (dp/(float)totalFrequency-frequencyMin)*graphHeight*(1.0/(frequencyMax-frequencyMin));
		if (rectHeight < 0) rectHeight = 0;
		if (rectHeight > graphHeight) rectHeight = graphHeight;

		dc.DrawRectangle(x, graphHeight-rectHeight, std::max(1, rectWidth), rectHeight);
	}
	PaintVertical(dc);
	PaintHorizontal(dc);
}

void HistBox::associatePlotBox(PlotBox *p){
	plot = p;
}

void HistBox::generate(double scaleMin, double scaleMax, double frequencyMin, double frequencyMax, int start, int end, int binWidth) {
	if (plot->standardFile.dataPoints.size() == 0) return;
	this->binWidth = binWidth;
	this->scaleMin = scaleMin;
	this->scaleMax = scaleMax;
	this->frequencyMin = frequencyMin;
	this->frequencyMax = frequencyMax;
	this->start = start;
	this->end = end;
	firstNonZero = lastNonZero = -1;
	totalFrequency = 0;
	freqSize = ((int)SHRT_MAX-(int)SHRT_MIN+1)/binWidth;
	if (freq != NULL) delete[] freq;
	freq = new int[freqSize];
	firstBucket = (plot->fromVoltage(scaleMin)-SHRT_MIN)/binWidth;
	lastBucket = (plot->fromVoltage(scaleMax)-SHRT_MIN)/binWidth+1;
	int highestBucket = 0;
	for (int i = 0; i < freqSize; i++) freq[i] = 0;
	for (int i = start; i < end; i++) {
		int dp = plot->standardFile.dataPoints[i];
		dp -= SHRT_MIN;
		freq[dp/binWidth]++;
		if (dp/binWidth > lastNonZero) lastNonZero = dp/binWidth;
		if (firstNonZero == -1 || dp/binWidth < firstNonZero) firstNonZero = dp/binWidth;
		if (freq[dp/binWidth] > highestBucket) highestBucket = freq[dp/binWidth];
		totalFrequency++;
	}
	if (frequencyMax <= 0) this->frequencyMax = highestBucket / (float)totalFrequency;
}

