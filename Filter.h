#ifndef _FILTER_
#define _FILTER_
class Filter;

#include <wx/wx.h>
#include <vector>
class PlotBox;

class Filter{
public:
        Filter();
        
        virtual wxString paramName();

        virtual void setParam(wxTextCtrl* ctrl);
        
        // Only called after createGUI, returns true if all the user-supplied data is valid, false otherwise
        // Should create it's own dialong to inform the user when the input is invalid
        virtual bool verifyInput(wxTextCtrl* ctrl);
        
        // Extracts the entered values from the GUI components
        // Only called after verifyInput
        virtual void harvestInput(wxTextCtrl* ctrl);
        
        // Applies the filter
        // Only called after harvestInput
        virtual void apply(PlotBox* plot, int start, int size);
        
};

#endif
