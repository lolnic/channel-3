#include "CustomDialog.h"

int getCustomDlgChoice(wxWindow *parent, wxString message, wxString caption, int n, wxString choices[]) {
	CustomDialog* dlg = new CustomDialog(parent, message, caption, n, choices);
	dlg->ShowModal();
	while (dlg->choice == -1) {
		wxGetApp().Yield();
	}
	dlg->EndModal(0);
	dlg->Close();
	return dlg->choice;
}

CustomDialog::CustomDialog(wxWindow *parent, wxString message, wxString caption, int n, wxString choices[])
		 : wxDialog(parent, -1, caption, wxDefaultPosition, wxDefaultSize,
		    wxSYSTEM_MENU | wxCAPTION | wxCLIP_CHILDREN), choice(-1)
{
	wxPanel *panel = new wxPanel(this, wxID_ANY);
	// Declare sizers
	wxFlexGridSizer *textSizer = new wxFlexGridSizer(4, 5, 10);
	wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer *mainSizer = new wxBoxSizer(wxVERTICAL);

	// Set up sizers
	panel->SetSizer(mainSizer);
	mainSizer->SetSizeHints(panel);
	mainSizer->Add(textSizer, 2, wxCENTER|wxTOP|wxLEFT|wxRIGHT, 20);
	mainSizer->Add(buttonSizer, 1, wxALIGN_RIGHT|wxTOP, 5);

	textSizer->Add(new wxStaticText(panel, wxID_ANY, message), 1, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);

	for (int i = 0; i < n; i++) {
		if (i == n-1) {
			buttonSizer->Add(new wxButton(panel, i, choices[i]), 1, wxLEFT|wxRIGHT|wxBOTTOM, 15);
		} else {
			buttonSizer->Add(new wxButton(panel, i, choices[i]), 1, wxLEFT|wxBOTTOM, 5);
		}
	}
	Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(CustomDialog::OnButton));

	panel->Fit();
	Fit();
	Center();
	Show(true);
}

void CustomDialog::OnButton(wxCommandEvent& event) {
	choice = event.GetId();
}
