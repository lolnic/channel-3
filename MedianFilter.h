#ifndef _MEDIAN_FILTER_
#define _MEDIAN_FILTER_

#include "Filter.h"
#include <algorithm>
#include <vector>

class MedianFilter : public Filter{
        long range;
public:
        MedianFilter();
        wxString paramName();
        void setParam(wxTextCtrl* ctrl);
        bool verifyInput(wxTextCtrl* ctrl);
        void harvestInput(wxTextCtrl* ctrl);
        void apply(PlotBox* plot, int start, int size);
};

#endif
