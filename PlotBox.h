#ifndef _PLOTBOX_
#define _PLOTBOX_

#define THRESHOLD_OPEN 0
#define THRESHOLD THRESHOLD_OPEN
// just in case I have to put the second threshold back in
#define THRESHOLD_CLOSED 2
#define BASELINE 1

class PlotBox;

#include <wx/wx.h>
#include <wx/ffile.h>
#include <vector>

typedef struct _fileStruct {
	// memory issues on large files made shorts necesary. Data points are 16-bit anyway
	std::vector<short> dataPoints;
	float gainfactor, interval, adcMin, adcMax;
	int databits, headerSize;
	bool signedData, signExtend, msb;
	wxString dataFilename;
	wxFFile dataFile;
	unsigned char *fileBytes;
	int dataLength;
	int fileVersion;;
} FileStruct;

#include "channel3.h"
#include <string>
#include <cstdio>
#include <wx/filename.h>
#include <wx/glcanvas.h>
#include <wx/config.h>
#include "ConfigurationWindow.h"
#include "Filter.h"
#include "PrintDialog.h"
#include "ScaleBar.h"
#include "versions.h"
#include "FileIO.h"
#include "HeaderFileIOv2.h"

// include OpenGL
#ifdef __WXMAC__
#include "OpenGL/glu.h"
#include "OpenGL/gl.h"
#else
#include <GL/glu.h>
#include <GL/gl.h>
#endif

// Delete this define to disable decimation
#define MAX_POINTS_PER_PLOT 16000

typedef struct _analysisEntry {
	int event;
	float openTime;
	float closeTime;
	float amplitude;
	float onTime;
	float offTime;
} AnalysisEntry;


class PlotBox : public wxGLCanvas{
	protected:
		// Event handlers
		void OnPaint(wxPaintEvent& event);
		void OnSize(wxSizeEvent& event);
		void OnEraseBackground(wxEraseEvent& event);
		void OnLeftClick(wxMouseEvent& event);

		// Protected methods
		void loadConfig();
		int file_size(wxFFile* file);
		bool file_good(wxFFile* file);
		bool onSameSide(float x, float y, float z);
		void prepare2DViewport(int topleft_x, int topleft_y, int bottomright_x, int bottomright_y);
		void loadPreferences();
		void writeAsText(wxString filename, bool append);
		int getFloatLength(float n);
		int getIntLength(int n);

		// Protected variables
		wxGLContext *context;
		wxScrollBar *scrollbarHorizontal;
		wxScrollBar *scrollbarVertical;
		ScaleBar* refreshingScale;
		wxTextCtrl* dropoutInput;
		bool readyToGL;
		FileIO* fileTypes[NVERSIONS+1];

	public:
		// Overrided methods
		PlotBox(wxWindow* parent, wxWindowID id, wxGLAttributes& args,
				wxPoint pos = wxDefaultPosition,
				wxSize size = wxDefaultSize, long style=0);
		~PlotBox();

		// Public methods
		void savePreferences();
		FileStruct* flagToFile(int flag);
		void openFile(wxString filename, int flag=USE_STANDARD_FSTRUCT);
		void saveFile(wxString filename, bool append);
		void saveAlternating(wxString filename, int requiredRemainder, bool append);
		void saveFirstMarks(wxString filename, bool append);
		void analyseData(wxString filename, MainWindow* win);
		wxString file();
		void adjustScrollbars();
		void centreAtCoord(int x);
		void centreAtDatapoint(int point);
		void loadConfigFromWindow(int flag=USE_STANDARD_FSTRUCT);
		void parseFileContents(int flag=USE_STANDARD_FSTRUCT);
		void associateScrollBars(wxScrollBar*, wxScrollBar*);
		void associateRefreshingScale(ScaleBar* scale);
		void setDropoutInput(wxTextCtrl* input);
		int getStartPoint();
		void setStartPoint(int start);
		void setPlotMin(int min);
		int getPlotMin();
		void setPlotRange(int range);
		int getPlotRange();
		void saveVScrollValues();
		void removeFilters(bool refresh = true);
		void scrollLeft();
		void scrollRight();
		int getCurrentMarker();
		void invertData();
		float toVoltage(int dataPoint, int flag=USE_STANDARD_FSTRUCT);
		int fromVoltage(double voltage, int flag=USE_STANDARD_FSTRUCT);

		// Public variables
		float displayMax, displayMin;
		FileStruct standardFile;
		FileStruct altFile;
		int viewSize, verticalViewSize;
		int dropOut;
		Filter *previewFilter;
		wxColour plotAltColour, plotForegroundColour, plotBackgroundColour, markerColour;
		bool plotWithLines;
		int defaultBaseline, defaultThresholdOpen, defaultThresholdClosed;
		std::vector<float> verticalMarkers;
		std::vector<int> horizontalMarkers;
		float lastAdcMin, lastAdcMax;

		friend class FileIO;
		friend class HeaderFileIOv1;
		friend class HeaderFileIOv2;
};

#endif
