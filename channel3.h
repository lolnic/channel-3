#ifndef _CHANNEL3_
#define _CHANNEL3_

#define USE_STANDARD_FSTRUCT 0
#define USE_ALT_FSTRUCT 1

#include <wx/wx.h>
#include <wx/aboutdlg.h>
#include <vector>
class MainWindow;

class Channel3 : public wxApp {
	virtual bool OnInit();
     std::vector<MainWindow*> win;
     void keyShortcut(wxKeyEvent&);
     DECLARE_EVENT_TABLE();
	
public:
	wxAboutDialogInfo* aboutDlgInfo;
	wxString lastFolder;
	
	void MacOpenFile(const wxString &fileName) ;
	void removeWindow (MainWindow* closedWin);
	void newWindow ();
};

DECLARE_APP(Channel3);

#endif
