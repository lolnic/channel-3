#include "MedianFilter.h"
#include "PlotBox.h"

MedianFilter::MedianFilter() : Filter(), range(1){      
}

wxString MedianFilter::paramName(){
        return _("Median Filter");
}

bool MedianFilter::verifyInput(wxTextCtrl* ctrl){
        long tmp;
        if(!ctrl->GetValue().ToLong(&tmp)){
                wxMessageBox(_("Range not a valid integer"));
                return false;
        }
        if(tmp % 2 != 1 || tmp < 1){
                wxMessageBox(_("Range is not odd or is less than 1"));
                return false;
        }
        return true;
}

void MedianFilter::setParam(wxTextCtrl* ctrl) {
        ctrl->Enable();
        ctrl->SetValue(_(""));
        *ctrl << range;
}

void MedianFilter::harvestInput(wxTextCtrl* ctrl){
        ctrl->GetValue().ToLong(&range);
}

// An efficient implementation of this would be a binary search tree
// This implementation is less likely to have bugs, but will be slower
// and may never finish on extremely large values of k (n is almost always extremely large)
// TODO: FIX THIS!
// But for now, O(nk log k) - Let's go!
void MedianFilter::apply(PlotBox* plot, int startpoint, int size){
        std::vector<int> window(range+1);
        std::vector<int> tempwindow;
        std::vector<short> result(plot->standardFile.dataPoints.size());
        for(int i = startpoint; i < startpoint + size; i++){
                window[i%range] = plot->standardFile.dataPoints[i];
                int actualrange = std::min((int)(i+range/2), startpoint+size-1) - std::max((int)(i-range/2), startpoint) + 1;
                tempwindow = window;
                std::sort(tempwindow.begin(), tempwindow.begin()+actualrange);
                if(actualrange % 2 == 0)
                        result[i] = (tempwindow[actualrange/2] + tempwindow[actualrange/2-1])/2;
                else
                        result[i] = tempwindow[actualrange/2];
        }
        plot->standardFile.dataPoints = result;
}