#include "GaussianFilter.h"
#include "PlotBox.h"

GaussianFilter::GaussianFilter() : Filter(), filterCutoff(1){      
}

wxString GaussianFilter::paramName(){
	return _("Gaussian Filter");
}

bool GaussianFilter::verifyInput(wxTextCtrl* ctrl){
	double tmp;
	if(!ctrl->GetValue().ToDouble(&tmp)){
		wxMessageBox(_("Filter cutoff not a valid decimal"));
		return false;
	}
	return true;
}

void GaussianFilter::setParam(wxTextCtrl* ctrl) {
	ctrl->Enable();
	ctrl->SetValue(_(""));
    *ctrl << filterCutoff;
}

void GaussianFilter::harvestInput(wxTextCtrl* ctrl){
	ctrl->GetValue().ToDouble(&filterCutoff);
}

void GaussianFilter::apply(PlotBox* plot, int startpoint, int size){
	std::vector<float> result(plot->standardFile.dataPoints.size());
	for (int i = 0; i < plot->standardFile.dataPoints.size(); i++) {
		result[i] = plot->standardFile.dataPoints[i];
	}
	a = exp(-14.5 * filterCutoff * plot->standardFile.interval / 1000000);
	int direction = 1;
	for (int i = 0; i < 4; i++) {
		int start, end;
		if (direction == 1) {
			start = startpoint+1;
			end = startpoint + size - 1;
		} else {
			start = startpoint+size-2;
			end = 0;
		}
		for (int x = start; x != end + direction; x += direction) {
			// implicitly reverse while filtering
			result[x] = result[x] * (1-a) + result[x-direction] * a;
		}
		direction = -direction;
	}
	for (int i = 0; i < result.size(); i++) {
		plot->standardFile.dataPoints[i] = result[i] + 0.5;
	}
}
