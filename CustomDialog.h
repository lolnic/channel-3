#ifndef _CUSTOMDIALOG_
#define _CUSTOMDIALOG_

#include "channel3.h"

int getCustomDlgChoice(wxWindow *parent, wxString message, wxString caption, int n, wxString choices[]);

class CustomDialog : public wxDialog {
	public:
		CustomDialog(wxWindow *parent, wxString message, wxString caption, int n, wxString choices[]);
		int choice;
	private:
		void OnButton(wxCommandEvent& event);
};

#endif
