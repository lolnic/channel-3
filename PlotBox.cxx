#include "PlotBox.h"
#include "channel3.h"
#include "MainWindow.h"
#include "config.h"
#include "CustomDialog.h"
#include <cstdlib>
#include <algorithm>
#include <iostream>

using std::cout;
using std::endl;

PlotBox::PlotBox(wxWindow* parent, wxWindowID id, wxGLAttributes& args, wxPoint pos, wxSize size, long style) :
	wxGLCanvas(parent, args, id, pos, size, style, _("")),
	displayMax(50.0), displayMin(-100.0),
	viewSize(500), previewFilter(NULL), verticalMarkers(2), readyToGL(false),
	dropOut(1), dropoutInput(NULL)
{
	loadPreferences();
	context = new wxGLContext(this);

	FileStruct * fstructs[2];
	fstructs[0] = &standardFile;
	fstructs[1] = &altFile;
	for (int i = 0; i < 2; i++) {
		fstructs[i]->gainfactor = 50.0;
		fstructs[i]->interval= 200.0;
		fstructs[i]->adcMin = lastAdcMin;
		fstructs[i]->adcMax = lastAdcMax;
		fstructs[i]->databits = 12;
		fstructs[i]->signedData = true;
		fstructs[i]->signExtend = true;
		fstructs[i]->msb = false;
		fstructs[i]->fileBytes = NULL;
		fstructs[i]->dataLength = 0;
		fstructs[i]->headerSize = 0;
	}
	fileTypes[0] = new FileIO;
	fileTypes[1] = new HeaderFileIOv2;

	Connect(wxEVT_PAINT, wxPaintEventHandler(PlotBox::OnPaint));
	Connect(wxEVT_SIZE, wxSizeEventHandler(PlotBox::OnSize));
	Connect(wxEVT_ERASE_BACKGROUND, wxEraseEventHandler(PlotBox::OnEraseBackground));
	Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(PlotBox::OnLeftClick));
}

void PlotBox::scrollLeft() {
	setStartPoint(getStartPoint()-viewSize);
	Refresh();
	refreshingScale->Refresh();
	Update();
	refreshingScale->Update();
}

void PlotBox::scrollRight() {
	setStartPoint(getStartPoint()+viewSize);
	Refresh();
	refreshingScale->Refresh();
	Update();
	refreshingScale->Update();
}

int PlotBox::getFloatLength(float n) {
	wxString result;
	result.Printf(_("%.2f"), n);
	return result.Length();
}

int PlotBox::getIntLength(int n) {
	wxString result;
	result.Printf(_("%d"), n);
	return result.Length();
}

FileStruct* PlotBox::flagToFile(int flag) {
	FileStruct* f = &standardFile;
	if (flag == USE_ALT_FSTRUCT) {
		f = &altFile;
	}
	return f;
}

float PlotBox::toVoltage (int n, int flag) {
	FileStruct *f = flagToFile(flag);
	float voltage = ((float)n * 20 * 1000) / (((1 << f->databits)-1) * f->gainfactor);
	return voltage;
}

int PlotBox::fromVoltage(double v, int flag) {
	FileStruct *f = flagToFile(flag);
	int n = v * (((1 << f->databits)-1) * f->gainfactor) / 20 / 1000;
	return n;
}

PlotBox::~PlotBox(){
	savePreferences();
}

void PlotBox::loadPreferences(){
	wxConfig *config = new wxConfig(_(APP_NAME));
	plotAltColour.Set(config->Read(_("/Colours/plotAlt"), _("#AAAAAA")));
	plotForegroundColour.Set(config->Read(_("/Colours/plotForeground"), _("#000000")));
	plotBackgroundColour.Set(config->Read(_("/Colours/plotBackground"), _("#ffffff")));
	markerColour.Set(config->Read(_("/Colours/marker"), _("#000000")));
	plotWithLines = config->Read(_("plotWithLines"), true);
	defaultBaseline = config->Read(_("/Markers/Baseline"), (long)0);
	defaultBaseline /= 1000;
	defaultThresholdOpen = config->Read(_("/Markers/ThresholdOpen"), (long)10000);
	defaultThresholdOpen /= 1000;
	lastAdcMin = config->Read(_("adcMin"), -50.0);
	lastAdcMax = config->Read(_("adcMax"), 40.0);
	delete config;
}

void PlotBox::savePreferences(){
	wxConfig *config = new wxConfig(_(APP_NAME));
	config->Write(_("/Colous/plotAlt"),         plotAltColour.GetAsString(wxC2S_HTML_SYNTAX));
	config->Write(_("/Colours/plotForeground"), plotForegroundColour.GetAsString(wxC2S_HTML_SYNTAX));
	config->Write(_("/Colours/plotBackground"), plotBackgroundColour.GetAsString(wxC2S_HTML_SYNTAX));
	config->Write(_("/Colours/marker"),         markerColour.GetAsString(wxC2S_HTML_SYNTAX));
	config->Write(_("plotWithLines"),           plotWithLines);
	config->Write(_("/Markers/Baseline"),       (long)(verticalMarkers[BASELINE]*1000));
	config->Write(_("/Markers/ThresholdOpen"),      (long)(verticalMarkers[THRESHOLD_OPEN]*1000));
	config->Write(_("adcMin"),					lastAdcMin);
	config->Write(_("adcMax"),					lastAdcMax);
	delete config;
}

void PlotBox::prepare2DViewport(int topleft_x, int topleft_y, int bottomright_x, int bottomright_y)
{
	wxGLCanvas::SetCurrent(*context);
	glEnable(GL_TEXTURE_2D);   // textures
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	glViewport(topleft_x, topleft_y, bottomright_x-topleft_x, bottomright_y-topleft_y);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluOrtho2D(topleft_x, bottomright_x, bottomright_y, topleft_y);
	glMatrixMode(GL_MODELVIEW);
}

// TODO:
// the X axis and Y axis are pretty confused in this function
// fix them
void PlotBox::OnPaint(wxPaintEvent& event){
	wxPaintDC dc(this);
	if(!IsShown())return;

	prepare2DViewport(0, 0, dc.GetSize().GetWidth(), dc.GetSize().GetHeight());
	readyToGL = true;

	/* previews disabled
	if(previewFilter != NULL) {
		standardFile.dataPoints = previewFilter->apply(this, getStartPoint(), std::min(viewSize, (int)standardFile.dataPoints.size()-getStartPoint()));
	}
	*/

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	float bgred = plotBackgroundColour.Red() / 255.0;
	float bggreen = plotBackgroundColour.Green() / 255.0;
	float bgblue = plotBackgroundColour.Blue() / 255.0;

	float fgred = plotForegroundColour.Red() / 255.0;
	float fggreen = plotForegroundColour.Green() / 255.0;
	float fgblue = plotForegroundColour.Blue() / 255.0;

	float altred = plotAltColour.Red() / 255.0;
	float altgreen = plotAltColour.Green() / 255.0;
	float altblue = plotAltColour.Blue() / 255.0;

	float markerred = markerColour.Red() / 255.0;
	float markergreen = markerColour.Green() / 255.0;
	float markerblue = markerColour.Blue() / 255.0;

	// background
	glColor4f(bgred, bggreen, bgblue, 1);
	glBegin(GL_QUADS);{
		glVertex3f(0, 0, 0);
		glVertex3f(GetSize().GetWidth(), 0, 0);
		glVertex3f(GetSize().GetWidth(), GetSize().GetHeight(), 0);
		glVertex3f(0, GetSize().GetHeight(), 0);
	}glEnd();


	FileStruct * fstructs[2];
	fstructs[0] = &standardFile;
	fstructs[1] = &altFile;
	for (int fs = 0; fs < 2; fs++) {
		FileStruct *f=fstructs[fs];
		if(f->dataPoints.size() <= 0){
			continue;
		}
		float mul = 1;
		if (fs == 1) {
			mul = fstructs[1]->interval/fstructs[0]->interval;
		}
		int lastx = 0;
		int lasty = 0;
		long double yaxis = 0;
		int height = dc.GetSize().GetHeight();
		int width = dc.GetSize().GetWidth();
		int endPoint = std::min((int)((getStartPoint() + viewSize)/mul), (int) f->dataPoints.size());
		int startPoint = getStartPoint()/mul;
		int plotRange = getPlotRange();
		int plotMin = getPlotMin();

		if (fs == 0) {
			glColor4f(fgred, fggreen, fgblue, 1);
		} else {
			glColor4f(altred, altgreen, altblue, 1);
		}
		if(plotWithLines) glBegin(GL_LINE_STRIP);
		else glBegin(GL_POINTS);
		{
			int visibleDropOut = dropOut;

#ifdef MAX_POINTS_PER_PLOT
			if ((endPoint - startPoint) / visibleDropOut > MAX_POINTS_PER_PLOT) {
				visibleDropOut = (endPoint - startPoint) / MAX_POINTS_PER_PLOT;
			}
#endif
			int beginOffset = startPoint % visibleDropOut;
			for(int i = startPoint - beginOffset; i < endPoint + visibleDropOut; i += visibleDropOut){
				if (i < 0 || i >= f->dataPoints.size()) continue;

				yaxis = ((long long)width * (long long)(i - startPoint) / (long double)(viewSize-1))*mul;
				float voltage = ((float)f->dataPoints[i] * 20 * 1000) / (((1 << f->databits)-1) * f->gainfactor);
				float xaxis = height-((voltage-plotMin)*height/(plotRange));
				glVertex3f(yaxis, xaxis, 0);
				lasty = yaxis;
				lastx = xaxis;
			}
		}glEnd();

		if (fs == 0) {
			glLineStipple(1, 0x00F0);
			glEnable(GL_LINE_STIPPLE);

			glColor4f(markerred, markergreen, markerblue, 1.0);
			glBegin(GL_LINES);{
				for(int i = 0; i < (int)verticalMarkers.size(); i++){
					float xaxis = height-((verticalMarkers[i]-getPlotMin())*height/(plotRange));
					glVertex3f(0, xaxis, 0);
					glVertex3f(width, xaxis, 0);
				}

				for(int i = 0; i < (int)horizontalMarkers.size(); i++) {
					float yaxis = ((float)horizontalMarkers[i] - getStartPoint()) * width / viewSize;
					glVertex3f(yaxis, 0, 0);
					glVertex3f(yaxis, height, 0);
				}
			}glEnd();
			glDisable(GL_LINE_STIPPLE);
		}
	}

	glFlush();
	SwapBuffers();
}

void PlotBox::OnSize(wxSizeEvent& event){
	//wxGLCanvas::OnSize(event);
	SetCurrent(*context);
	if(readyToGL)
		prepare2DViewport(0, 0, GetSize().GetWidth(), GetSize().GetHeight());
}

void PlotBox::OnEraseBackground(wxEraseEvent& event){
}

void PlotBox::OnLeftClick(wxMouseEvent& event){
	event.Skip();
	centreAtCoord(event.GetX());
	Refresh();
	refreshingScale->Refresh();
}

// Public methods

void PlotBox::associateScrollBars(wxScrollBar* scrollHorizontal, wxScrollBar* scrollVertical){
	scrollbarHorizontal = scrollHorizontal;
	scrollbarVertical = scrollVertical;
	adjustScrollbars();
}

void PlotBox::associateRefreshingScale(ScaleBar* scale) {
	refreshingScale = scale;
}

int PlotBox::getStartPoint(){
	if(scrollbarHorizontal == NULL)return 0;
	return scrollbarHorizontal->GetThumbPosition();
}

void PlotBox::setStartPoint(int start){
	if(scrollbarHorizontal != NULL)
		scrollbarHorizontal->SetThumbPosition(std::min((int)std::max(standardFile.dataPoints.size(), altFile.dataPoints.size()) - viewSize, std::max(start,0)));
}

int PlotBox::getPlotMin(){
	if(scrollbarVertical == NULL)return 0;
	return (scrollbarVertical->GetRange() - scrollbarVertical->GetThumbPosition()) - verticalViewSize - (1<<(std::max(standardFile.databits, altFile.databits)-1));
}

void PlotBox::setPlotMin(int min){
	if(scrollbarVertical != NULL)
		scrollbarVertical->SetThumbPosition(scrollbarVertical->GetRange() - min - (1<<(std::max(standardFile.databits, altFile.databits)-1)) - verticalViewSize);
}

int PlotBox::getPlotRange(){
	return verticalViewSize;
}

void PlotBox::setPlotRange(int range){
	verticalViewSize = range;
	if(scrollbarVertical != NULL)
		scrollbarVertical->SetScrollbar(
				scrollbarVertical->GetThumbPosition(),
				verticalViewSize,
				(1<<(std::max(standardFile.databits, altFile.databits)-1)),
				scrollbarVertical->GetPageSize()
				);
}

void PlotBox::saveVScrollValues(){
	displayMin = getPlotMin();
	displayMax = displayMin + getPlotRange();
}

void PlotBox::removeFilters(bool refresh){
	previewFilter = NULL;
	parseFileContents();
	//standardFile.dataPoints = standardFile.fileContents;
	if(refresh){
		Refresh();
		Update();
	}
}

void PlotBox::adjustScrollbars(){

	if(scrollbarHorizontal != NULL){
		// This is most likely called after a zoom, which means that the dimensions might be weird
		// This line prevents segmentation faults when the view gets into an invalid location
		int fcSize = std::max(standardFile.dataPoints.size(),  altFile.dataPoints.size());
		setStartPoint(std::max(0, std::min(getStartPoint(), (int)fcSize-viewSize)));

		scrollbarHorizontal->SetScrollbar(
				std::min(getStartPoint(), fcSize-viewSize),
				std::min(viewSize, fcSize),
				fcSize,
				viewSize/2
				);
	}

	if(scrollbarVertical != NULL){
		scrollbarVertical->SetScrollbar(
				scrollbarVertical->GetThumbPosition(),
				getPlotRange(),
				1<<std::max(standardFile.databits, altFile.databits),
				(displayMax - displayMin)/10
				);
	}

}

void PlotBox::setDropoutInput (wxTextCtrl* input) {
	dropoutInput = input;
}

// TODO:
// A horrible function.
// Needs reorganising.
void PlotBox::openFile(wxString filename, int flag){
	FileStruct * f=flagToFile(flag);
	if(f->dataFile.IsOpened()){
		// Ensure that the file is actually discarded
		if(f->dataFile.IsOpened())
			f->dataFile.Close();
		delete[] f->fileBytes;
		f->fileBytes = NULL;
		f->dataPoints.clear();
		f->dataFilename = _("");
		f->dataLength = 0;
		adjustScrollbars();
		f->dataFile.Close();
	}

	if (flag == USE_STANDARD_FSTRUCT) {
		verticalMarkers[THRESHOLD_OPEN] = defaultThresholdOpen;
		verticalMarkers[BASELINE] = defaultBaseline;
		horizontalMarkers.clear();
		previewFilter = NULL;
		dropOut = 1;
		if(dropoutInput != NULL) dropoutInput->SetValue(_("1"));
	}

	f->dataFile.Open(filename, _("rb"));
	if(!file_good(&(f->dataFile))){
		// wxWidgets shows the user it's own 'File does not exist' dialog
		Refresh();
		return;
	}

	if(f->fileBytes != NULL){
		delete[] f->fileBytes;
		f->fileBytes = NULL;
	}


	f->dataFilename = filename;
	int filesize = file_size(&(f->dataFile));

	f->fileBytes = new unsigned char[filesize*2];
	f->dataLength = filesize;
	f->dataFile.Read(f->fileBytes, filesize);
	f->fileVersion = versionNumber(f->fileBytes, filesize);

	fileTypes[f->fileVersion]->loadConfig(this, filesize, flag);
	f->headerSize = fileTypes[f->fileVersion]->read(this, filesize, flag);

	viewSize = std::min((int)std::max(standardFile.dataPoints.size(), altFile.dataPoints.size()) , 500);
	setStartPoint(0);
	adjustScrollbars();
	scrollbarVertical->SetThumbPosition((1<<(int)(std::max(standardFile.databits-standardFile.signedData, altFile.databits-altFile.signedData)))-(int)displayMax);
	Refresh();
}

void PlotBox::writeAsText(wxString filename, bool append) {
	wxString mode = _("wb");
	if (append) {
		mode = _("ab");
	}
	wxFFile out(filename, mode);
	for (int i = 0; i < standardFile.dataPoints.size(); i+=dropOut) {
		wxString line;
		float voltage = ((float)standardFile.dataPoints[i] * 20 * 1000) / (((1 << standardFile.databits)-1) * standardFile.gainfactor);
		line.Printf(_("%f %f\r\n"), standardFile.interval * i, voltage);
		out.Write(line);
	}
	out.Close();
}

void PlotBox::saveFile(wxString filename, bool append) {
	if (previewFilter != NULL) {
		if (wxMessageBox(_("You are currently previewing a filter. The data written to the file will not match the data you currently see. Continue?"),
					_("Write to file?"), wxYES_NO) == wxNO) return;
	}
	wxFileName tmp;
	wxString extension;
	tmp.SplitPath(filename, NULL, NULL, &extension);
	if (extension.Lower() == _("txt")) {
		writeAsText(filename, append);
		return;
	}
	fileTypes[NVERSIONS]->write(this, filename, append, standardFile.dataPoints);
}

void PlotBox::saveAlternating(wxString filename, int requiredRemainder, bool append) {
	// copy the data points
	std::vector<short> oldDataPoints = standardFile.dataPoints;
	standardFile.dataPoints.clear();
	// copy the markers
	std::vector<int> oldMarkers = horizontalMarkers;
	std::vector<int> tmpMarkers;
	tmpMarkers.push_back(0);
	for (int i = 0; i < horizontalMarkers.size(); i++) {
		tmpMarkers.push_back(horizontalMarkers[i]);
	}
	tmpMarkers.push_back(oldDataPoints.size());
	horizontalMarkers.clear();

	for (int i = requiredRemainder; i < tmpMarkers.size() - 1; i += 2) {
		for (int j = tmpMarkers[i]; j < tmpMarkers[i+1]; j ++) {
			standardFile.dataPoints.push_back(oldDataPoints[j]);
		}
	}

	saveFile(filename, append);

	standardFile.dataPoints = oldDataPoints;
	horizontalMarkers = oldMarkers;
	Refresh();
	Update();
}

void PlotBox::saveFirstMarks(wxString filename, bool append) {
	// copy the data points
	std::vector<short> oldDataPoints = standardFile.dataPoints;
	standardFile.dataPoints.clear();
	// copy the markers
	std::vector<int> oldMarkers = horizontalMarkers;
	horizontalMarkers.clear();

	for (int i = oldMarkers[0]; i < oldMarkers[1]; i++) {
		standardFile.dataPoints.push_back(oldDataPoints[i]);
	}

	saveFile(filename, append);

	standardFile.dataPoints = oldDataPoints;
	horizontalMarkers = oldMarkers;
}

#define FIX_COLUMN_LENGTH(x) for (int i = 0; i < table.size(); i++) { \
	x##ColumnLength = std::max(x##ColumnLength, getFloatLength(table[i].x)); \
}

// Return true if x and y are both less than or greater than z
bool PlotBox::onSameSide(float x, float y, float z) {
	if (x <= z && y <= z)
		return true;
	if (x >= z && y >= z)
		return true;
	return false;
}

void PlotBox::analyseData (wxString filename, MainWindow *win) {
	wxFFile outFile(filename, _("w"));
	std::vector<AnalysisEntry> table;

	int pos = 0;
	float lastClose = 0;
	int eventNumber = 1;
	while (pos < standardFile.dataPoints.size()) {
		AnalysisEntry currentEntry;

		float openTime, closeTime;
		float amplitude = 0;
		while (pos < standardFile.dataPoints.size() && onSameSide(toVoltage(standardFile.dataPoints[pos]), verticalMarkers[BASELINE], verticalMarkers[THRESHOLD])) {
			pos++;
		}
		openTime = pos * standardFile.interval / 1000;
		while (pos < standardFile.dataPoints.size() && !onSameSide(toVoltage(standardFile.dataPoints[pos]), verticalMarkers[BASELINE], verticalMarkers[THRESHOLD])) {
			pos++;
			amplitude += toVoltage(standardFile.dataPoints[pos]);
		}
		closeTime = pos * standardFile.interval / 1000;
		amplitude /= ((closeTime-openTime)/standardFile.interval*1000);
		amplitude -= verticalMarkers[BASELINE];

		currentEntry.event = eventNumber;
		currentEntry.openTime = openTime;
		currentEntry.closeTime = closeTime;
		currentEntry.offTime = openTime - lastClose;
		currentEntry.onTime = closeTime - openTime;
		currentEntry.amplitude = amplitude;
		table.push_back(currentEntry);

		lastClose = closeTime;
		eventNumber++;
	}

	int eventColumnLength = strlen("event");
	int openTimeColumnLength = strlen("open");
	int closeTimeColumnLength = strlen("close");
	int offTimeColumnLength = strlen("closed time");
	int onTimeColumnLength = strlen("open time");
	int amplitudeColumnLength = strlen("amplitude");

	for (int i = 0; i < table.size(); i++) {
		eventColumnLength = std::max(eventColumnLength, getIntLength(table[i].event));
	}
	FIX_COLUMN_LENGTH(openTime);
	FIX_COLUMN_LENGTH(closeTime);
	FIX_COLUMN_LENGTH(offTime);
	FIX_COLUMN_LENGTH(onTime);
	FIX_COLUMN_LENGTH(amplitude);

	wxString headings;
	headings.Printf(_("%*s  %*s  %*s  %*s  %*s  %*s\n"),
			eventColumnLength, _("event"),
			openTimeColumnLength, _("open"),
			closeTimeColumnLength, _("close"),
			offTimeColumnLength, _("closed time"),
			onTimeColumnLength, _("open time"),
			amplitudeColumnLength, _("amplitude"));
	outFile.Write(headings);

	float totalOpenTime = 0, totalCloseTime = 0, openProbability;
	bool wasOpenNan = false, wasCloseNan = false;
	int nEvents = table.size();

	for (int i = 0; i < table.size(); i++) {
		if (table[i].onTime == table[i].onTime) { // openTime is not nan
			totalOpenTime += table[i].onTime;
		} else {
			wasOpenNan = true;;
		}
		if (table[i].offTime == table[i].offTime) { // closetime is not nan
			totalCloseTime += table[i].offTime;
		} else {
			wasCloseNan = true;
		}
		wxString event, open, close, off, on, amplitude;
		event.Printf(_("%*d  "), eventColumnLength, table[i].event);
		open.Printf(_("%*.2f  "), openTimeColumnLength, table[i].openTime);
		close.Printf(_("%*.2f  "), closeTimeColumnLength, table[i].closeTime);
		off.Printf(_("%*.2f  "), offTimeColumnLength, table[i].offTime);
		on.Printf(_("%*.2f  "), onTimeColumnLength, table[i].onTime);
		amplitude.Printf(_("%*.2f  "), amplitudeColumnLength, table[i].amplitude);

		wxString output;
		output = event + open + close + off + on + amplitude + _("\n");

		outFile.Write(output);
	}
	double avgOpenTime = 0, avgCloseTime = 0;
	if (nEvents-wasOpenNan > 0) {
		avgOpenTime = totalOpenTime / (nEvents-wasOpenNan);
	}
	if (nEvents-wasCloseNan > 0) {
		avgCloseTime = totalCloseTime / (nEvents-wasCloseNan);
	}
	outFile.Close();

	openProbability = totalOpenTime/(totalOpenTime+totalCloseTime);
	wxString stats, fstats, fheadings;
	wxFileName fname;
	wxString outName;
	fname.SplitPath(file(), NULL, &outName, NULL);
	stats.Printf(_("File:\t\t\t\t\t\t%s\nTotal Open Time:\t\t%.2lf\nAverage Open Time:\t\t%.2f\nTotal Close Time:\t\t%.2lf\nAverage Close Time:\t\t%.2f\nOpen Probability:\t\t%.4f\nNumber of Events:\t\t%d\n"),
			outName.c_str(), totalOpenTime, avgOpenTime, totalCloseTime, avgCloseTime, openProbability, nEvents);
	wxString saveChoices[] = {_("Discard"), _("Save")};
	if(getCustomDlgChoice(NULL, _("Some statistics from the analysis:\n") + stats  + _("\nDo you want to save them?"), _("Statistics"), 2, saveChoices) == 0) {
		return;
	}
	fstats.Printf(_("%s\t%.2lf\t\t%.2f\t\t%.2lf\t\t%.2f\t\t%.4f\t%d\n"), outName.c_str(), totalOpenTime, avgOpenTime, totalCloseTime, avgOpenTime, openProbability, nEvents);
	fheadings = _("File\tTotal open\tAvg. Open\tTotal Close\tAvg. Close\tP(Open)\t#Events\n");
	wxString outStr = fheadings + fstats;
	wxString mode = _("w");
	wxFileDialog fileChooser(NULL, _("Choose where to save"), wxGetApp().lastFolder, _(""), _("All Files (*)|*"), wxFD_SAVE);
	wxString path, dir;
	switch(fileChooser.ShowModal()) {
		case wxID_CANCEL:
			return;
		default:
			path = fileChooser.GetPath();
			fname.SplitPath(path, NULL, &dir, NULL, NULL, wxPATH_NATIVE);
	}
	if (!fname.IsDirWritable(dir)) {
		wxMessageBox(_("You don't have permission to write to this directory"));
		return;
	}
	if (fname.FileExists(path)) {
		wxString appendChoices[] = {_("Overwrite"), _("Append")};
		if (getCustomDlgChoice(NULL, _("Do you want to append to existing file?"), _("File Exists"), 2, appendChoices) == 1) {
			outStr = fstats;
			mode = _("a");
		}
	}

	wxGetApp().lastFolder = dir;

	wxFFile out2(path, mode);
	out2.Write(outStr);
	out2.Close();
}

void PlotBox::invertData() {
	for (int i = 0; i < standardFile.dataPoints.size(); i++) {
		standardFile.dataPoints[i] = -standardFile.dataPoints[i];
	}
	for (int i = 0; i < altFile.dataPoints.size(); i++) {
		altFile.dataPoints[i] = -altFile.dataPoints[i];
	}
}

void PlotBox::parseFileContents(int flag){
	FileStruct* f = flagToFile(flag);
	f->dataPoints.clear();

	wxGauge *gauge = new wxGauge(scrollbarHorizontal->GetParent(), wxID_ANY, f->dataLength/2, scrollbarHorizontal->GetPosition(), scrollbarHorizontal->GetSize());
	scrollbarHorizontal->Hide();

	for(int i = f->headerSize/2; i < f->dataLength/2; i++){
		// Convert integers in the file to native byte order through multiplication
		unsigned char firstByte, secondByte;
		bool mustNegate = false;
		if(f->msb){
			firstByte = f->fileBytes[i*2];
			secondByte = f->fileBytes[i*2+1];
		}
		else{
			firstByte = f->fileBytes[i*2+1];
			secondByte = f->fileBytes[i*2];
		}
		if(firstByte & (1<<7) && f->signedData){ // Number is negative
			mustNegate = true;
			firstByte = ~firstByte;
			secondByte = ~secondByte;
		}
		f->dataPoints.push_back(((int)firstByte)*256 + secondByte);
		if(mustNegate){
			f->dataPoints[i-f->headerSize/2]++;
			f->dataPoints[i-f->headerSize/2] = -f->dataPoints[i-f->headerSize/2];
		}
		// Updating the progress bar too frequently causes slowdowns
		// The progress bar's value is only changed every 80000 iterations
		// This value is obtained through trial and error and provides smooth
		// animation as well as speed
		if(i%80000 == 0){
			gauge->SetValue(i);
			gauge->Refresh();
			wxGetApp().Yield(true);
		}
	}
	gauge->Hide();
	scrollbarHorizontal->Show();
	scrollbarHorizontal->Refresh();
	scrollbarVertical->Refresh();
	scrollbarHorizontal->Update();
	scrollbarVertical->Update();
}

int PlotBox::getCurrentMarker() {
	for (int i = 0; i < horizontalMarkers.size(); i++) {
		if (horizontalMarkers[i] > getStartPoint()) {
			return i;
		}
	}
	return horizontalMarkers.size();
}

wxString PlotBox::file(){
	return standardFile.dataFilename;
}

void PlotBox::centreAtCoord(int x){
	centreAtDatapoint(getStartPoint() + x/(GetSize().GetWidth()/(float)this->viewSize));
}

void PlotBox::centreAtDatapoint(int point){
	setStartPoint(std::max(0, std::min(point-viewSize/2, (int)std::max(standardFile.dataPoints.size(), altFile.dataPoints.size())-viewSize)));
	adjustScrollbars();
}

void PlotBox::loadConfigFromWindow(int flag){
	FileStruct *f = flagToFile(flag);
	ConfigurationWindow * configwin = new ConfigurationWindow(this, this);
	while(configwin->IsShown())
		wxGetApp().Yield(true);

	configwin->Close();
	f->gainfactor = configwin->gainfactor;
	f->interval = configwin->interval;
	displayMax = configwin->displayMax;
	displayMin = configwin->displayMin;
	f->adcMin = configwin->adcMin;
	f->adcMax = configwin->adcMax;
	f->databits = configwin->databits;
	f->signedData = configwin->signedData;
	f->signExtend = configwin->signExtend;
	f->msb = configwin->msb;
	f->headerSize = configwin->headerSize;
	adjustScrollbars();
}


// Private methods

// Load the configuration. If it's a *.DAT file,
// pop up the configuration window, otherwise read
// from the header of the file
void PlotBox::loadConfig(){
	loadConfigFromWindow();
	verticalViewSize = (displayMax - displayMin);
	setPlotMin(displayMin);
}

// Returns true if the file exists, false otherwise
bool PlotBox::file_good(wxFFile* file){
	return file->IsOpened();

}

// Return the size of the file, function will
// likely crash if the file doesn't exist.
int PlotBox::file_size(wxFFile* file){
	return file->Length();
}
