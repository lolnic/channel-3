#include "Filter.h"
#include "PlotBox.h"

Filter::Filter(){

}

wxString Filter::paramName(){
	return _("No Filter");
}

bool Filter::verifyInput(wxTextCtrl* ctrl){
	return true;
}

void Filter::setParam(wxTextCtrl* ctrl) {
	ctrl->Disable();
	ctrl->SetValue(_(""));
}

void Filter::harvestInput(wxTextCtrl* ctrl){
}

void Filter::apply(PlotBox* plot, int start, int size){
}
