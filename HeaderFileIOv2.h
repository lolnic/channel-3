#ifndef _HEADERFILEV2_
#define _HEADERFILEV2_
class HeaderFileIOv2;

#include <wx/wx.h>
#include <vector>
#include "FileIO.h"
#include "PlotBox.h"

class HeaderFileIOv2 : public FileIO {
public:
     void loadConfig(PlotBox* plot, int binaryLength, int flag=USE_STANDARD_FSTRUCT);
     int read(PlotBox* plot, int binaryLength, int flag=USE_STANDARD_FSTRUCT);
     void write(PlotBox* plot, wxString filename, bool append, std::vector<short>& data, int flag=USE_STANDARD_FSTRUCT);
};

#endif
