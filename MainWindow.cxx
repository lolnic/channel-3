#include "MainWindow.h"
#include "CustomDialog.h"
#include "Filter.h"
#include "AverageFilter.h"
#include "MedianFilter.h"
#include "GaussianFilter.h"
#include "PlotBoxBaseline.h"

// Event enum and table
enum{
	ID_Open=1,
	ID_SaveAs,
	ID_SaveOddEven,
	ID_SaveEvenOdd,
	ID_SaveFirstMarks,
	ID_Quit,
	ID_Filters,
	ID_ConfigurationWindow,
	ID_ToggleFullScreen,
	ID_Scroll,
	ID_Vscroll,
	ID_Preferences,
	ID_Dropout,
	ID_Goto,
	ID_Timer,
	ID_MarkerSpin,
	ID_ClearMarkers,
	ID_About,
	ID_Analyse,
	ID_NewWindow,
	ID_ZoomHorizontal,
	ID_ToggleInvert,
	ID_OpenAlt,
	ID_Reduces,
	ID_Print,
	ID_Histogram,
	ID_NoFilter,
	ID_AverageFilter,
	ID_GaussianFilter,
	ID_MedianFilter,
	ID_FilterInput
};

	BEGIN_EVENT_TABLE(MainWindow, wxFrame)
		EVT_MENU(ID_Quit, MainWindow::OnQuit)
		EVT_MENU(ID_Open, MainWindow::OnOpenFile)
		EVT_MENU(ID_OpenAlt, MainWindow::OnOpenAltFile)
		EVT_MENU(ID_SaveAs, MainWindow::OnSaveAs)
		EVT_MENU(ID_SaveOddEven, MainWindow::OnSaveOddEven)
		EVT_MENU(ID_SaveEvenOdd, MainWindow::OnSaveEvenOdd)
		EVT_MENU(ID_SaveFirstMarks, MainWindow::OnSaveFirstMarks)
		EVT_MENU(ID_Reduces, MainWindow::OnReduces)
		EVT_MENU(ID_ConfigurationWindow, MainWindow::OnConfigurationWindow)
		EVT_MENU(ID_ToggleFullScreen, MainWindow::OnToggleFullScreen)
		EVT_MENU(ID_ToggleInvert, MainWindow::OnToggleInvert)
		EVT_MENU(ID_Preferences, MainWindow::OnPreferences)
		EVT_MENU(ID_About, MainWindow::OnAbout)
		EVT_MENU(ID_Analyse, MainWindow::OnAnalyse)
		EVT_MENU(ID_NewWindow, MainWindow::OnNewWindow)
		EVT_MENU(ID_Print, MainWindow::OnPrint)
		EVT_MENU(ID_Histogram, MainWindow::OnHistogram)
		EVT_MENU(ID_NoFilter, MainWindow::OnNoFilter)
		EVT_MENU(ID_AverageFilter, MainWindow::OnAverageFilter)
		EVT_MENU(ID_MedianFilter, MainWindow::OnMedianFilter)
		EVT_MENU(ID_GaussianFilter, MainWindow::OnGaussionFilter)

		EVT_COMMAND_SCROLL(ID_ZoomHorizontal, MainWindow::OnZoomHorizontal)
		EVT_BUTTON(ID_ClearMarkers, MainWindow::OnClearMarkers)

		EVT_TEXT_ENTER(ID_Dropout, MainWindow::OnDropoutChange)
		EVT_TEXT_ENTER(ID_Goto, MainWindow::OnGotoChange)
		EVT_TEXT_ENTER(ID_FilterInput, MainWindow::OnFilterParamChange)

		EVT_TIMER(ID_Timer, MainWindow::updateInterval)

		EVT_SPINCTRL(ID_MarkerSpin, MainWindow::OnMarkerSpin)
		EVT_TEXT(ID_MarkerSpin, MainWindow::OnMarkerChange)

		EVT_CLOSE(MainWindow::OnClose)

	END_EVENT_TABLE();

void MainWindow::ChangeFilter(Filter* toApply) {
	filterParamName->SetLabel(toApply->paramName());
	if (currentFilter != toApply) {
		toApply->setParam(filterParamInput);
	}
	currentFilter = toApply;
	if(!currentFilter->verifyInput(filterParamInput)) return;
	currentFilter->harvestInput(filterParamInput);
	plotPanel->removeFilters();
	currentFilter->apply(this->plotPanel->getPlot(), 0, plotPanel->getPlot()->standardFile.dataPoints.size());
	Refresh();
}

void MainWindow::OnNoFilter(wxCommandEvent& event) {
	ChangeFilter(filters[0]);
}

void MainWindow::OnAverageFilter(wxCommandEvent& event) {
	ChangeFilter(filters[1]);
}

void MainWindow::OnMedianFilter(wxCommandEvent& event) {
	ChangeFilter(filters[2]);
}

void MainWindow::OnGaussionFilter(wxCommandEvent& event) {
	ChangeFilter(filters[3]);
}

void MainWindow::OnFilterParamChange(wxCommandEvent& event) {
	ChangeFilter(currentFilter);
}

int MainWindow::fileDialog(int ftype, wxString& filePath, long style) {
	assert(style == wxFD_OPEN || style == wxFD_SAVE);
	int mode = OVERWRITE;
	std::vector<wxString> extensions;
	wxString fileType = _("");
	bool notFirst = false;
	if (ftype & DAT_FILES) {
		fileType += _("DAT Files (*.dat)|*.dat;*.DAT");
		extensions.push_back(_(".dat"));
		notFirst = true;
	}
	if (ftype & TXT_FILES) {
		if (notFirst) fileType += _("|");
		fileType += _("Text Files (*.txt)|*.txt;*.TXT");
		extensions.push_back(_(".txt"));
		notFirst = true;
	}
	if (ftype & PRN_FILES) {
		if (notFirst) fileType += _("|");
		fileType += _("PRN Files (*.prn)|*.prn;*.PRN");
		extensions.push_back(_(".prn"));
		notFirst = true;
	}
	if (ftype & ALL_FILES) {
		if (notFirst) fileType += _("|");
		fileType += _("All Files (*)|*");
		extensions.push_back(_(""));
	}


	// Choose the appropriate prompt
	wxString prompt = _("Choose where to save");
	if (style == wxFD_OPEN) {
		prompt = _("Choose a file");
	}
	wxFileName fname;
	wxString directory, extension, path;

	wxString initialDirectory = wxGetApp().lastFolder;

	if (plotPanel->getPlot()->standardFile.dataFilename != _("") && style == wxFD_SAVE) {
		fname.SplitPath(plotPanel->getPlot()->standardFile.dataFilename, NULL, &initialDirectory, NULL, NULL, wxPATH_NATIVE);
	}

	// Get file selection from user
	wxFileDialog fileChooser(this->panel, prompt, initialDirectory, _(""), fileType, style);
	switch(fileChooser.ShowModal()){
		case  wxID_CANCEL: // The file choosing was canceled
			return false;
		default: // A file was chosen, tell the PlotBox which one.
			path = fileChooser.GetPath();
			fname.SplitPath(path, NULL, &directory, NULL, &extension, wxPATH_NATIVE);
	}

	// Validate selection
	if (style == wxFD_SAVE) {
		if (extension == _("")) {
			path += extensions[fileChooser.GetFilterIndex()];
		}
		if (!fname.IsDirWritable (directory)) {
			wxMessageBox(_("You don't have permission to write to this directory"));
			return false;
		}
		if (fname.FileExists (path)) {
			int choice = getCustomDlgChoice(NULL, _("The file already exists."), _("FIle Exists"), 3, (wxString[]){_("Overwrite"), _("Append"), _("Cancel")});
			if (choice != 2) {
				if (!fname.IsFileWritable (path)) {
					wxMessageBox(_("You don't have permission to write to this file"));
					return false;
				}
				if (choice == 1) mode = APPEND;
			} else {
				return false;
			}
		}
	} else if (style == wxFD_OPEN) {
		if (!fname.IsFileReadable (path)) {
			wxMessageBox(_("You don't have permission to read from this file"));
			return false;
		}
	}

	wxGetApp().lastFolder = directory;
	filePath = path;
	return mode;
}

void MainWindow::OnNewWindow(wxCommandEvent& event) {
	wxGetApp().newWindow();
}

// This function is called when the "Open File" menu option is chosen.
// It starts up a native file chooser
// and sends the chosen file to the PlotBox, to be
// loaded and rendered next time the PlotBox is asked to draw itself
void MainWindow::OnOpenFile(wxCommandEvent& WXUNUSED(event)){
	wxString path;
	if (fileDialog (DAT_FILES|ALL_FILES, path, wxFD_OPEN)) {
		plotPanel->getPlot()->openFile (path);
	}

	if(plotPanel->getPlot()->file() != _("")){
		wxString name;
		wxFileName tmp;
		tmp.SplitPath(plotPanel->getPlot()->file(), NULL, NULL, &name, NULL);
		SetTitle(name + _(" - ") + _(FULL_APP_NAME));

		if (histWindow != NULL) {
			histWindow->Close();
			histWindow = NULL;
		}
	}

	adjustZoomControls();

	Refresh();
	Update();
}

void MainWindow::OnOpenAltFile(wxCommandEvent& WXUNUSED(event)) {
	wxString path;
	if (fileDialog (DAT_FILES|ALL_FILES, path, wxFD_OPEN)) {
		plotPanel->getPlot()->openFile(path, USE_ALT_FSTRUCT);
	}
	adjustZoomControls();
}

void MainWindow::OnDropoutChange(wxCommandEvent& event) {
	wxString dropoutStr = dropoutInput->GetValue();
	long dropout;
	if (!dropoutStr.ToLong(&dropout)) {
		dropoutInput->SetForegroundColour(*wxRED);
		return;
	}
	if (dropout <= 0) {
		dropoutInput->SetForegroundColour(*wxRED);
	} else {
		dropoutInput->SetForegroundColour(wxNullColour);
		plotPanel->getPlot()->dropOut = dropout;
		Refresh();
		Update();
		plotPanel->getPlot()->SetFocus();
	}
}

void MainWindow::OnGotoChange(wxCommandEvent& event) {
	wxString gotoStr = gotoInput->GetValue();
	double position;
	long filePosition;
	if(!gotoStr.ToDouble(&position)) {
		gotoInput->SetForegroundColour(*wxRED);
		return;
	}
	filePosition = position*1000/plotPanel->getPlot()->standardFile.interval;
	if(filePosition < plotPanel->getPlot()->viewSize/2 || filePosition > plotPanel->getPlot()->standardFile.dataPoints.size()) {
		gotoInput->SetForegroundColour(*wxRED);
	} else {
		gotoInput->SetForegroundColour(wxNullColour);
		plotPanel->getPlot()->centreAtDatapoint(filePosition);
		plotPanel->getPlot()->SetFocus();
		Refresh();
		Update();
		lastGotoVal = position;
	}
}

void MainWindow::markerChange () {
#if defined(__WXMSW__) || defined(__WXMAC__)
	if (ignoreNextMarkerEvent) {
		ignoreNextMarkerEvent = false;
		return;
	}
#endif
	int currentMarker = markerInput->GetValue() - 1;
	if (currentMarker <= -1) {
		plotPanel->getPlot()->setStartPoint(0);
	} else if (currentMarker < plotPanel->getPlot()->horizontalMarkers.size()) {
		plotPanel->getPlot()->setStartPoint(plotPanel->getPlot()->horizontalMarkers[currentMarker]);
	}

	Refresh();
	Update();
}

void MainWindow::OnMarkerSpin(wxSpinEvent& event) {
	markerChange();
}

void MainWindow::OnMarkerChange(wxCommandEvent& event) {
	markerChange();
}

void MainWindow::updateGotoBox () {
	if (FindFocus() != gotoInput) {
		int middle = plotPanel->getPlot()->getStartPoint() + plotPanel->getPlot()->viewSize / 2;
		if (middle != lastGotoVal) {
			gotoInput->Clear();
			wxString tmp;
			tmp.Printf(_("%.2f"), (middle*plotPanel->getPlot()->standardFile.interval/1000));
			gotoInput->ChangeValue(tmp);
			lastGotoVal = middle;
			gotoInput->SetForegroundColour(wxNullColour);
		}
	}
	if (FindFocus() != dropoutInput) {
		int dropout = plotPanel->getPlot()->dropOut;
		wxString tmp;
		tmp.Printf(_("%d"), dropout);
		dropoutInput->ChangeValue(tmp);
		dropoutInput->SetForegroundColour(wxNullColour);
	}
}

void MainWindow::updateInterval(wxTimerEvent& event) {
	if (fileWaiting) {
		fileWaiting = false;
		plotPanel->getPlot()->openFile(waitingFile);
		if(plotPanel->getPlot()->file() != _("")){
			wxString name;
			wxFileName tmp;
			tmp.SplitPath(plotPanel->getPlot()->file(), NULL, NULL, &name, NULL);
			SetTitle(name + _(" - ") + _(FULL_APP_NAME));
		}

		adjustZoomControls();

		Refresh();
		Update();
	}
	updateGotoBox();

	markerInput->SetRange(0, plotPanel->getPlot()->horizontalMarkers.size());

	if (markerInput->GetValue() != plotPanel->getPlot()->getCurrentMarker()) {
#ifdef __WXMAC__
		ignoreNextMarkerEvent = true;
#endif
		markerInput->SetValue(plotPanel->getPlot()->getCurrentMarker());
	}
	wxString tmp;
	tmp.Printf(_("%.2f"), plotPanel->getPlot()->verticalMarkers[THRESHOLD_OPEN]);
	if (openDisplay->GetLabel() != tmp) openDisplay->SetLabel(tmp);
	tmp.Printf(_("%.2f"), plotPanel->getPlot()->verticalMarkers[BASELINE]);
	if (baseDisplay->GetLabel() != tmp) baseDisplay->SetLabel(tmp);
	tmp.Printf(_("%.2f"), plotPanel->getPlot()->verticalMarkers[THRESHOLD_OPEN] - plotPanel->getPlot()->verticalMarkers[BASELINE]);
	if (diffDisplay->GetLabel() != tmp) diffDisplay->SetLabel(tmp);
	if (plotPanel->getFocussedMarker() == -1) {
		tmp = _("");
	} else {
		tmp.Printf(_("%.2f ms"), (float)plotPanel->getPlot()->horizontalMarkers[plotPanel->getFocussedMarker()]/1000*plotPanel->getPlot()->standardFile.interval);
	}
	if (markerPosDisplay->GetLabel() != tmp) markerPosDisplay->SetLabel(tmp);
}

void MainWindow::loadWhenReady(wxString file) {
	fileWaiting = true;
	waitingFile = file;
}

void MainWindow::OnSaveAs(wxCommandEvent& WXUNUSED(event)){
	wxString path;
	int mode;
	if (mode=fileDialog (DAT_FILES|TXT_FILES|ALL_FILES, path, wxFD_SAVE)) {
		if (mode == OVERWRITE) {
			plotPanel->getPlot()->saveFile (path, false);
		} else {
			plotPanel->getPlot()->saveFile (path, true);
		}
	}

	if(plotPanel->getPlot()->file() != _("")){
		wxString name;
		wxFileName tmp;
		tmp.SplitPath(plotPanel->getPlot()->file(), NULL, NULL, &name, NULL);
		SetTitle(name + _(" - ") + _(FULL_APP_NAME));
	}
}

void MainWindow::OnSaveOddEven(wxCommandEvent& event) {
	if (plotPanel->getPlot()->horizontalMarkers.size() < 1) {
		wxMessageBox(_("There are no markers"));
	} else {
		wxString path;
		int mode;
		if (mode=fileDialog (DAT_FILES|TXT_FILES|ALL_FILES, path, wxFD_SAVE)) {
			if (mode == OVERWRITE) {
				plotPanel->getPlot()->saveAlternating (path, 1, false);
			} else {
				plotPanel->getPlot()->saveAlternating (path, 1, true);
			}
		}
	}
}

void MainWindow::OnSaveEvenOdd(wxCommandEvent& event) {
	if (plotPanel->getPlot()->horizontalMarkers.size() < 1) {
		wxMessageBox(_("There are no markers"));
	} else {
		wxString path;
		int mode;
		if (mode = fileDialog (DAT_FILES|TXT_FILES|ALL_FILES, path, wxFD_SAVE)) {
			if (mode == OVERWRITE) {
				plotPanel->getPlot()->saveAlternating (path, 0, false);
			} else {
				plotPanel->getPlot()->saveAlternating (path, 0, true);
			}
		}
	}
}

void MainWindow::OnSaveFirstMarks(wxCommandEvent& event) {
	if (plotPanel->getPlot()->horizontalMarkers.size() < 2) {
		wxMessageBox(_("You need at least two markers"));
	} else {
		wxString path;
		int mode;
		if (mode=fileDialog (DAT_FILES|TXT_FILES|ALL_FILES, path, wxFD_SAVE)) {
			if (mode == OVERWRITE) {
				plotPanel->getPlot()->saveFirstMarks (path, false);
			} else{
				plotPanel->getPlot()->saveFirstMarks (path, true);
			}
		}
	}
}

void MainWindow::OnAnalyse (wxCommandEvent& event) {
	wxString path;
	if (fileDialog (PRN_FILES|ALL_FILES, path, wxFD_SAVE)) {
		plotPanel->getPlot()->analyseData (path, this);
	}
}

void MainWindow::OnFilters(wxCommandEvent& WXUNUSED(event)){

}

void MainWindow::OnPrint(wxCommandEvent& event) {
	printWindow->Show(true);
	printWindow->SetFocus();
	printWindow->Raise();
}

void MainWindow::OnHistogram(wxCommandEvent& event) {
	if (histWindow == NULL) {
		histWindow = new Histogram(this, this->plotPanel->getPlot());
		histWindow->win = this;
	}
	histWindow->Show(true);
	histWindow->SetFocus();
	histWindow->Raise();
}

void MainWindow::OnReduces(wxCommandEvent& WXUNUSED(event)) {
	reduceWindow->updateSpinners();
	reduceWindow->updateStats();
	reduceWindow->Show(true);
	reduceWindow->SetFocus();
	reduceWindow->Raise();
}

// This function is called when the "Configuration Window" menu item is chosen.
// It makes the plotbox create a ConfigurationWindow and load the values in
void MainWindow::OnConfigurationWindow(wxCommandEvent& WXUNUSED(event)){
	plotPanel->getPlot()->loadConfigFromWindow();
	plotPanel->getPlot()->parseFileContents();
	plotPanel->getPlot()->verticalViewSize = (plotPanel->getPlot()->displayMax - plotPanel->getPlot()->displayMin);
	plotPanel->getPlot()->setPlotMin(plotPanel->getPlot()->displayMin);
	plotPanel->getPlot()->adjustScrollbars();

	Refresh();
	Update();
}

// This function is called when the "Full Screen" menu item is chosen
// It puts the main window into full screen mode
void MainWindow::OnToggleFullScreen(wxCommandEvent& WXUNUSED(event)){
	this->ShowFullScreen(!this->IsFullScreen(), wxFULLSCREEN_NOBORDER);
}

void MainWindow::OnToggleInvert(wxCommandEvent& WXUNUSED(event)) {
	plotPanel->getPlot()->invertData();
	plotPanel->getPlot()->Refresh();
	plotPanel->getPlot()->Update();
}

void MainWindow::OnAbout(wxCommandEvent& event) {
	wxAboutBox(*(wxGetApp().aboutDlgInfo));
}

void MainWindow::adjustZoomControls() {
	horizontalZoom->SetRange(10*log2(500), 10*log2(std::max(plotPanel->getPlot()->standardFile.dataPoints.size(), plotPanel->getPlot()->altFile.dataPoints.size())));
	horizontalZoom->SetPageSize(5);
	horizontalZoom->SetValue(0);
	plotPanel->adjustZoomControls();
	updateViewSizeDisplay();
}

void MainWindow::updateViewSizeDisplay () {
	wxString tmp;
	tmp.Printf(_("%.0f ms"), plotPanel->getPlot()->viewSize*plotPanel->getPlot()->standardFile.interval/1000);
	if (tmp != viewSizeDisplay->GetLabel()) viewSizeDisplay->SetLabel(tmp);
}

void MainWindow::OnZoomHorizontal(wxScrollEvent& WXUNUSED(event)){
	int zoomSize = pow(2, (horizontalZoom->GetValue()/10.0)) - plotPanel->getPlot()->viewSize;
	if(plotPanel->getPlot()->viewSize + zoomSize >= 500){
		plotPanel->getPlot()->setStartPoint(plotPanel->getPlot()->getStartPoint()-zoomSize/2);
		plotPanel->getPlot()->viewSize += zoomSize;
		updateViewSizeDisplay();
		plotPanel->getPlot()->adjustScrollbars();
		Refresh();
	}
}

void MainWindow::OnClearMarkers (wxCommandEvent& event) {
	plotPanel->clearMarkers();
	Refresh();
	Update();
}

void MainWindow::OnPreferences(wxCommandEvent& event){
	new PreferencesDialog(this->panel, plotPanel, tabBar, staticTexts, horizontalZoom);
}

	MainWindow::MainWindow(const wxString& title, const wxPoint& pos, const wxSize& size)
: wxFrame(NULL, -1, title, pos, size, wxFULL_REPAINT_ON_RESIZE | wxDEFAULT_FRAME_STYLE),
  fileWaiting(false), ignoreNextMarkerEvent(true), histWindow(NULL), lastGotoVal(0)
{
	// Start the timer
	updateTimer = new wxTimer(this, ID_Timer);

	// Create the menu
	// File
	wxMenu *menuFile = new wxMenu;
	menuFile->Append(ID_NewWindow, _("&New Window\tCtrl+N"));
	menuFile->Append(ID_Open, _("&Open\tCtrl+O"));
	menuFile->Append(ID_OpenAlt, _("Open &Comparison File"));
	menuFile->Append(ID_SaveAs, _("Save Whole File &As\tCtrl+Shift+S"));
	menuFile->Append(ID_SaveOddEven, _("Save Between O&dd and Even Markers"));
	menuFile->Append(ID_SaveEvenOdd, _("Save Between &Even and Odd Markers"));
	menuFile->Append(ID_SaveFirstMarks, _("Save Between &First Two Markers"));
	menuFile->Append(ID_Print, _("&Print\tCtrl+P"));
	menuFile->Append(ID_Quit, _("E&xit\tAlt+F4"));

	// Edit
	wxMenu *menuEdit = new wxMenu;
	//menuEdit->Append(ID_Filters, _("&Filters\tAlt+F"));
	menuEdit->Append(ID_Reduces, _("&Statistics\tAlt+S"));
	menuEdit->Append(ID_ConfigurationWindow, _("&Sample Properties\tAlt+L"));
#ifndef __WXMSW__
	menuEdit->Append(ID_Preferences, _("Preferences"));
#endif
	// View
	wxMenu *menuView = new wxMenu;
	menuView->Append(ID_ToggleFullScreen, _("&Fullscreen\tF11"));
	menuView->Append(ID_ToggleInvert, _("&Invert Data\tCtrl+I"));
	menuView->Append(ID_Histogram, _("Amplitude &Histogram\tCtrl+H"));

	// Filters
	wxMenu *menuFilter = new wxMenu;
	menuFilter->Append(ID_NoFilter, _("&No Filter"), _(""), wxITEM_RADIO);
	menuFilter->Append(ID_AverageFilter, _("&Average Filter"), _(""), wxITEM_RADIO);
	menuFilter->Append(ID_MedianFilter, _("&Median Filter"), _(""), wxITEM_RADIO);
	menuFilter->Append(ID_GaussianFilter, _("&Gaussian Filter"), _(""), wxITEM_RADIO);

	nFilters = 4;
	filters = new Filter*[nFilters];
	filters[0] = new Filter;
	filters[1] = new AverageFilter;
	filters[2] = new MedianFilter;
	filters[3] = new GaussianFilter;
	currentFilter = filters[0];

	// Tools
	wxMenu *menuTools = new wxMenu;
	menuTools->Append(ID_Analyse, _("Open/Close Analysis\tAlt+C"));
#ifdef __WXMSW__
	menuTools->Append(ID_Preferences, _("Options"));
#endif

	// Help
	wxMenu* menuHelp = new wxMenu;
	menuHelp->Append(ID_About, _("About"));

	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append(menuFile, _("File"));
	menuBar->Append(menuEdit, _("Edit"));
	menuBar->Append(menuView, _("View"));
	menuBar->Append(menuFilter, _("Filters"));
	menuBar->Append(menuTools, _("Tools"));
	SetMenuBar(menuBar);

	// Create a panel to hold the widgets
	panel = new wxPanel(this, wxID_ANY);
	wxBoxSizer *mainSizer = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *topControlSizer = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer *topZoomSizer = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *plotTabBarSizer = new wxBoxSizer(wxHORIZONTAL);

	panel->SetSizer(mainSizer);
	mainSizer->SetSizeHints(panel);

	plotPanel = new PlotPanel(panel);

	mainSizer->Add(topControlSizer, 0, wxLEFT, PLOT_MARGIN);
	mainSizer->Add(topZoomSizer, 0, wxLEFT|wxEXPAND, PLOT_MARGIN);
	mainSizer->Add(plotTabBarSizer, 1, wxLEFT|wxEXPAND, 0);

	// Create the zoom controls
	horizontalZoom = new wxSlider(panel, ID_ZoomHorizontal, 500, 500, 1000, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);


	wxButton *markerClearButton = new wxButton(panel, ID_ClearMarkers, _("Clear Markers"));
	filterParamName = new wxStaticText(panel, wxID_ANY, currentFilter->paramName(), wxDefaultPosition, wxSize(90, -1));
	viewSizeDisplay = new wxStaticText(panel, wxID_ANY, _("0 ms"), wxDefaultPosition, wxSize(66, -1));
	openDisplay = new wxStaticText(panel, wxID_ANY, _(""), wxDefaultPosition, wxSize(50, -1));
	closedDisplay = new wxStaticText(panel, wxID_ANY, _(""), wxDefaultPosition, wxSize(50, -1));
	baseDisplay = new wxStaticText(panel, wxID_ANY, _(""), wxDefaultPosition, wxSize(50, -1));
	diffDisplay = new wxStaticText(panel, wxID_ANY, _(""), wxDefaultPosition, wxSize(50, -1));
	markerPosDisplay = new wxStaticText(panel, wxID_ANY, _(""), wxDefaultPosition, wxSize(-1, -1));
	filterParamInput = new wxTextCtrl(panel, ID_FilterInput, _(""), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
	staticTexts.push_back(new wxStaticText(panel, wxID_ANY, _("Dropout: ")));
	staticTexts.push_back(new wxStaticText(panel, wxID_ANY, _("Display Midpoint:")));
	staticTexts.push_back(new wxStaticText(panel, wxID_ANY, _("Marker:")));
	staticTexts.push_back(new wxStaticText(panel, wxID_ANY, _("View Size: ")));
	staticTexts.push_back(viewSizeDisplay);
	staticTexts.push_back(new wxStaticText(panel, wxID_ANY, _("T: ")));
	staticTexts.push_back(openDisplay);
	staticTexts.push_back(new wxStaticText(panel, wxID_ANY, _("B: ")));
	staticTexts.push_back(baseDisplay);
	staticTexts.push_back(new wxStaticText(panel, wxID_ANY, _("T-B: ")));
	staticTexts.push_back(diffDisplay);
	staticTexts.push_back(new wxStaticText(panel, wxID_ANY, _("M: ")));
	staticTexts.push_back(markerPosDisplay);

	// Create the text boxes
	dropoutInput = new wxTextCtrl (panel, ID_Dropout, _("1"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
	gotoInput = new wxTextCtrl (panel, ID_Goto, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);

	// Create the marker spinner
	markerInput = new wxSpinCtrl (panel, ID_MarkerSpin, _("0"));

	// Create the TabBar
	tabBar = new TabBar(panel, wxID_ANY, wxDefaultPosition,
			wxDefaultSize, wxFULL_REPAINT_ON_RESIZE);
	tabBar->associatePlotBox(this->plotPanel->getPlot());

	plotTabBarSizer->Add(plotPanel, 1, wxEXPAND);
	plotTabBarSizer->Add(tabBar, 0, wxEXPAND|wxBOTTOM, PLOT_MARGIN);

	topZoomSizer->Add(horizontalZoom, 0, wxEXPAND|wxRIGHT, 50);
	topControlSizer->Add(filterParamName, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT);
	topControlSizer->Add(filterParamInput, 0, wxALIGN_CENTER_VERTICAL);
	topControlSizer->Add(staticTexts[0], 0, wxALIGN_CENTER_VERTICAL);
	topControlSizer->Add(dropoutInput, 0, wxALIGN_CENTER_VERTICAL);
	topControlSizer->Add(staticTexts[1], 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10);
	topControlSizer->Add(gotoInput, 0, wxALIGN_CENTER_VERTICAL);
	topControlSizer->Add(staticTexts[2], 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10);
	topControlSizer->Add(markerInput, 0, wxALIGN_CENTER_VERTICAL);
	topControlSizer->Add(markerClearButton, 0, wxALIGN_CENTER_VERTICAL);
	for (int i = 3; i < staticTexts.size(); i++) {
		if (i%2== 1) {
			topControlSizer->Add(staticTexts[i], 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10);
		} else {
			topControlSizer->Add(staticTexts[i], 1, wxALIGN_CENTER_VERTICAL);
		}
	}

	// Prepare the Printer window
	printWindow = new PrintDialog(this, this->plotPanel->getPlot());

	// Prepare the reduce window
	reduceWindow = new ReduceDialog(this, this->plotPanel->getPlot());

	updateTimer->Start(50);
}

void MainWindow::OnClose(wxCloseEvent& event) {
	// Stop timers
	updateTimer->Stop();
	// Tell the app the window is closed
	wxGetApp().removeWindow(this);
	// Shut. Down. Everything.
	Destroy();
}

void MainWindow::OnQuit(wxCommandEvent& WXUNUSED(event)){
	Close(true);
}
