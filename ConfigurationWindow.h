#ifndef _CONFIGWINDOW_
#define _CONFIGWINDOW_

#include "channel3.h"
#include "PlotBox.h"
#include <sstream>

class ConfigurationWindow : public wxDialog {
	public:
		ConfigurationWindow(wxWindow* parent, PlotBox * plot, int flag=USE_STANDARD_FSTRUCT);
		double gainfactor, interval, displayMax, displayMin, adcMin, adcMax;
		long databits, headerSize;
		bool signedData, signExtend, msb;
	private:
		FileStruct *fs;
		wxTextCtrl *gainfactorInput, *intervalInput, *displayMaxInput, *headerSizeInput,
			   *displayMinInput, *databitsInput, *adcminInput, *adcmaxInput;
		wxCheckBox *signeddataCheck;
		wxComboBox *bitorderBrowser;
		wxButton *doneButton;
          PlotBox *plot;

		void OnDoneButton(wxCommandEvent& event);
		bool allEntriesAreValid();
		DECLARE_EVENT_TABLE();
};

#endif
