#include "TabBar.h"

void TabBar::markerDimensions(int marker, int& x, int& y, int& w, int& h, wxDC* dc){
	double minimum, maximum, totalsize;
	minimum = plot->getPlotMin();
	maximum = plot->getPlotMin() + plot->getPlotRange();
	totalsize = maximum-minimum;
	int height = dc->GetTextExtent(_("0")).GetHeight();

	x = 0;
	y = dc->GetSize().GetHeight() - ((plot->verticalMarkers[marker] - minimum) / totalsize * dc->GetSize().GetHeight()) - height/2;
	w = dc->GetSize().GetWidth()/3*(3-marker);
	h = height;
}    

void TabBar::loadPreferences(){
	wxConfig *config = new wxConfig(_(APP_NAME));
	foregroundColour.Set(config->Read(_("/Colours/scaleForeground"), _("#000000")));
	backgroundColour.Set(config->Read(_("/Colours/scaleBackground"), _("#ffffff")));
	delete config;
}

TabBar::TabBar(wxPanel *parent, int id, wxPoint pos, wxSize size, long style) :
	wxPanel(parent, id, pos, size, style), plot(NULL), moving(false), movingAll(false), focussedMarker(-1)
{
	loadPreferences();
	backgroundBrush = new wxBrush(backgroundColour);
	foregroundPen = new wxPen(foregroundColour);
	markerBrush = new wxBrush(foregroundColour);
	markerPen = new wxPen(foregroundColour);

	Connect(wxEVT_PAINT, wxPaintEventHandler(TabBar::OnPaint));
	Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(TabBar::OnLeftClick));
	Connect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(TabBar::OnRightClick));
	Connect(wxEVT_MOTION, wxMouseEventHandler(TabBar::OnMouseMove));
	Connect(wxEVT_LEFT_UP, wxMouseEventHandler(TabBar::OnLeftUp));
	Connect(wxEVT_RIGHT_UP, wxMouseEventHandler(TabBar::OnRightUp));
}

void TabBar::OnPaint(wxPaintEvent& event){
	wxPaintDC dc(this);
	dc.SetBackground(*backgroundBrush);
	dc.SetTextForeground(foregroundColour);
	dc.SetPen(*foregroundPen);
	dc.Clear();
	if(plot == NULL)return;

	wxColour* markerColours[2] = {new wxColour(255, 0, 0), new wxColour(255, 255, 0)};
	wxString names[2] = {_("T"), _("B"),};
	for(int i = 0; i < 2; i++){
		markerBrush->SetColour(*markerColours[i]);
		markerPen->SetColour(*markerColours[i]);
		if (focussedMarker == i) {
			dc.SetPen(*wxBLACK);
		} else {
			dc.SetPen(*markerPen);
		}
		dc.SetBrush(*markerBrush);
		int dx, dy, dw, dh;
		markerDimensions(i, dx, dy, dw, dh, &dc);
		dc.DrawRectangle(dx, dy, dw, dh);
		dc.DrawText(names[i], dw - dc.GetTextExtent(names[i]).GetWidth(), dy);
	}
}

void TabBar::OnLeftClick(wxMouseEvent& event){
	if (movingAll) return;
	int tabHit = -1;
	wxClientDC dc(this);
	int cx, cy; // cursor coordinates
	cx = event.GetLogicalPosition(dc).x;
	cy = event.GetLogicalPosition(dc).y;
	for(int i = 0; i < (int)plot->verticalMarkers.size(); i++){
		int mx, my, mw, mh; // marker coordinates
		markerDimensions(i, mx, my, mw, mh, &dc);
		if(mx <= cx && cx < mx+mw && my <= cy && cy < my+mh){
			tabHit = i;
		}
	}
	if(tabHit != -1){
		moving = true;
		movingMarker = tabHit;
		focussedMarker = tabHit;
		Refresh();
		Update();
	}
}

void TabBar::OnRightClick(wxMouseEvent& event) {
	if (moving) return;
	wxClientDC dc(this);
	startLoc = event.GetLogicalPosition(dc).y;
	movingAll = true;
}

void TabBar::OnMouseMove(wxMouseEvent& event){
	if(event.Dragging() && moving){
		wxClientDC dc(this);
		int cx, cy;
		cx = event.GetLogicalPosition(dc).x;
		cy = event.GetLogicalPosition(dc).y;
		plot->verticalMarkers[movingMarker] = 
			plot->getPlotRange()-((float)cy)/dc.GetSize().GetHeight()*plot->getPlotRange() + plot->getPlotMin();
		Refresh();
		plot->Refresh();
		Update();
		plot->Update();
	}
	if (event.Dragging() && movingAll) {
		wxClientDC dc(this);
		float origLoc = plot->getPlotRange()-((float)startLoc)/dc.GetSize().GetHeight()*plot->getPlotRange() + plot->getPlotMin();
		float curLoc = plot->getPlotRange()-((float)event.GetLogicalPosition(dc).y)/dc.GetSize().GetHeight()*plot->getPlotRange() + plot->getPlotMin();
		for (int i = 0; i < plot->verticalMarkers.size(); i++) {
			plot->verticalMarkers[i] += curLoc - origLoc;
		}
		startLoc = event.GetLogicalPosition(dc).y;
		Refresh();
		plot->Refresh();
		Update();
		plot->Update();
	}
}

void TabBar::OnLeftUp(wxMouseEvent& event){
	moving = false;
}

void TabBar::OnRightUp(wxMouseEvent& event) {
	movingAll = false;
}

void TabBar::shiftMarker(int amount) {
	if (focussedMarker != -1) {
		plot->verticalMarkers[focussedMarker] += amount;
		plot->Refresh();
		Refresh();
		plot->Update();
		Update();
	}
}

void TabBar::associatePlotBox(PlotBox *p){
	plot = p;
}

bool TabBar::SetForegroundColour(const wxColour& colour){
	if(!colour.IsOk()) return false;
	foregroundColour = colour;
	foregroundPen->SetColour(foregroundColour);
	return true;
}

wxColour TabBar::GetForegroundColour(){
	return foregroundColour;
}

bool TabBar::SetBackgroundColour(const wxColour& colour){
	if(!colour.IsOk())return false;
	backgroundColour = colour;
	backgroundBrush->SetColour(backgroundColour);
	return true;
}

wxColour TabBar::GetBackgroundColour(){
	return backgroundColour;
}
