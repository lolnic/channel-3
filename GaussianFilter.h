#ifndef _GAUSSIAN_FILTER_
#define _GAUSSIAN_FILTER_

#include "Filter.h"
#include <algorithm>

class GaussianFilter : public Filter{
        double filterCutoff, a;
public:
        GaussianFilter();
        wxString paramName();
        void setParam(wxTextCtrl* ctrl);
        bool verifyInput(wxTextCtrl* ctrl);
        void harvestInput(wxTextCtrl* ctrl);
        void apply(PlotBox* plot, int start, int size);
};

#endif
