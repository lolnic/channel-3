#ifndef _PRINTWINDOW_
#define _PRINTWINDOW_

class PrintDialog;

#include "channel3.h"
#include "PlotBox.h"
#include "PlotPrintout.h"
#include <wx/spinctrl.h>

class PrintDialog : public wxFrame{
	public:
		PrintDialog(wxWindow *parent, PlotBox* plot);
	private:
		PlotBox *plot;
		wxPanel *panel;
		wxSpinCtrl *pageSizeInput, *dropoutInput;
		wxTextCtrl *commentInput, *titleInput, *lineSizeInput, *startInput, *endInput;

		void OnPrint(wxCommandEvent& event);
		void OnCancel(wxCommandEvent& event);
		void OnPreview(wxCommandEvent& event);

		DECLARE_EVENT_TABLE();
};

#endif
