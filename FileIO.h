#ifndef _FILEIO_
#define _FILEIO_

class PlotBox;

#include <wx/wx.h>
#include <vector>
#include "channel3.h"

class FileIO {
public:
     virtual void loadConfig(PlotBox* plot, int binaryLength, int flag=USE_STANDARD_FSTRUCT);
     virtual int read(PlotBox* plot, int binaryLength, int flag=USE_STANDARD_FSTRUCT);
     virtual void write(PlotBox* plot, wxString filename, bool append, std::vector<short>& data, int flag=USE_STANDARD_FSTRUCT);
};

#endif
