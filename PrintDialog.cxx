#include "PrintDialog.h"
#define STREOF "#EndOfFile"

BEGIN_EVENT_TABLE(PrintDialog, wxFrame)
	EVT_BUTTON(wxID_OK, PrintDialog::OnPrint)
	EVT_BUTTON(wxID_APPLY, PrintDialog::OnPreview)
	EVT_BUTTON(wxID_CANCEL, PrintDialog::OnCancel)
END_EVENT_TABLE()

PrintDialog::PrintDialog(wxWindow *parent, PlotBox *plot)
	: wxFrame(parent, -1, _("Printing"), wxDefaultPosition, wxDefaultSize,
			wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxSYSTEM_MENU | wxCAPTION | wxCLIP_CHILDREN),
	plot(plot)
{
	panel = new wxPanel(this, wxID_ANY);
	wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

	sizer->SetSizeHints(panel);
	panel->SetSizer(sizer);

	wxStaticBoxSizer *optionsSizer = new wxStaticBoxSizer(wxVERTICAL, panel);
	sizer->Add(optionsSizer, 1, wxEXPAND|wxLEFT|wxRIGHT, 5);
	wxBoxSizer * tmpSizer;

	wxFileName fname;
	wxString outName;
	fname.SplitPath(plot->file(), NULL, &outName, NULL);

	tmpSizer = new wxBoxSizer(wxHORIZONTAL);
	tmpSizer->Add(new wxStaticText(panel, -1, _("Title"), wxDefaultPosition, wxSize(150, -1)), 0, wxALIGN_CENTER|wxLEFT, 5);
	tmpSizer->Add(titleInput = new wxTextCtrl(panel, -1, _(""), wxDefaultPosition, wxDefaultSize), 1, wxEXPAND|wxLEFT|wxRIGHT, 5);
	titleInput->SetValue(outName);
	optionsSizer->Add(tmpSizer, 0, wxEXPAND);

	tmpSizer = new wxBoxSizer(wxHORIZONTAL);
	tmpSizer->Add(new wxStaticText(panel, -1, _("Seconds per line"), wxDefaultPosition, wxSize(150, -1)), 0, wxALIGN_CENTER|wxLEFT, 5);
	tmpSizer->Add(lineSizeInput = new wxTextCtrl(panel, -1, _("30"), wxDefaultPosition, wxDefaultSize, wxTE_RIGHT), 1, wxEXPAND|wxLEFT|wxRIGHT, 5);
	optionsSizer->Add(tmpSizer, 0, wxEXPAND);

	tmpSizer = new wxBoxSizer(wxHORIZONTAL);
	tmpSizer->Add(new wxStaticText(panel, -1, _("Lines per page"), wxDefaultPosition, wxSize(150, -1)), 0, wxALIGN_CENTER|wxLEFT, 5);
	tmpSizer->Add(pageSizeInput = new wxSpinCtrl(panel, -1, _("4")), 1, wxEXPAND|wxLEFT|wxRIGHT, 5);
	optionsSizer->Add(tmpSizer, 0, wxEXPAND);
	pageSizeInput->SetRange(1, 100);

	tmpSizer = new wxBoxSizer(wxHORIZONTAL);
	tmpSizer->Add(new wxStaticText(panel, -1, _("Start time (s)"), wxDefaultPosition, wxSize(150, -1)), 0, wxALIGN_CENTER|wxLEFT, 5);
	tmpSizer->Add(startInput = new wxTextCtrl(panel, -1, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_RIGHT), 1, wxEXPAND|wxLEFT|wxRIGHT, 5);
	optionsSizer->Add(tmpSizer, 0, wxEXPAND);

	tmpSizer = new wxBoxSizer(wxHORIZONTAL);
	tmpSizer->Add(new wxStaticText(panel, -1, _("End time (s)"), wxDefaultPosition, wxSize(150, -1)), 0, wxALIGN_CENTER|wxLEFT, 5);
	tmpSizer->Add(endInput = new wxTextCtrl(panel, -1, _(STREOF), wxDefaultPosition, wxDefaultSize, wxTE_RIGHT), 1, wxEXPAND|wxLEFT|wxRIGHT, 5);
	optionsSizer->Add(tmpSizer, 1, wxEXPAND);

	tmpSizer = new wxBoxSizer(wxHORIZONTAL);
	tmpSizer->Add(new wxStaticText(panel, -1, _("Dropout"), wxDefaultPosition, wxSize(150, -1)), 0, wxALIGN_CENTER|wxLEFT, 5);
	tmpSizer->Add(dropoutInput = new wxSpinCtrl(panel, -1, _("1"), wxDefaultPosition, wxDefaultSize, wxTE_RIGHT), 1, wxEXPAND|wxLEFT|wxRIGHT, 5);
	dropoutInput->SetValue(plot->dropOut);
	optionsSizer->Add(tmpSizer, 1, wxEXPAND);

	tmpSizer = new wxBoxSizer(wxHORIZONTAL);
	tmpSizer->Add(new wxStaticText(panel, -1, _("Comment"), wxDefaultPosition, wxSize(150, -1)), 0, wxALIGN_CENTER|wxLEFT, 5);
	tmpSizer->Add(commentInput = new wxTextCtrl(panel, -1, _("")), 1, wxEXPAND|wxLEFT|wxRIGHT, 5);
	optionsSizer->Add(tmpSizer, 1, wxEXPAND);
	wxStdDialogButtonSizer *buttonSizer = new wxStdDialogButtonSizer();
	buttonSizer->AddButton(new wxButton(panel, wxID_OK, _("Print")));
	buttonSizer->AddButton(new wxButton(panel, wxID_APPLY, _("Preview")));
	buttonSizer->AddButton(new wxButton(panel, wxID_CANCEL, _("Cancel")));
	buttonSizer->Realize();
	sizer->Add(buttonSizer, 0, wxSHAPED);

	panel->Fit();
	Fit();
	Centre();
}

void PrintDialog::OnPrint(wxCommandEvent& event) {
	int pointsPerLine, linesPerPage, start, end, dropout;
	double spl, istart, iend;
	if (!lineSizeInput->GetValue().ToDouble(&spl) || spl <= 0) {
		wxMessageBox(_("Seconds per line must be a positive number"));
		return;
	}
	if (!startInput->GetValue().ToDouble(&istart) || istart < 0) {
		wxMessageBox(_("Start time must be a non-negative number"));
		return;
	}
	if ((!endInput->GetValue().ToDouble(&iend) || iend <= 0) && endInput->GetValue() != _(STREOF)) {
		wxMessageBox(_("End time must be a positive number"));
		return;
	}
	pointsPerLine = spl * 1000000 / plot->standardFile.interval;
	linesPerPage = pageSizeInput->GetValue();
	start = istart * 1000000 / plot->standardFile.interval;
	if (endInput->GetValue() == _(STREOF))
		end = plot->standardFile.dataPoints.size();
	else
		end = iend * 1000000 / plot->standardFile.interval;
	if (start >= end) {
		wxMessageBox(_("End time must be after start time"));
		return;
	}
	dropout = dropoutInput->GetValue();
	wxString comment = commentInput->GetValue();
	wxString title = titleInput->GetValue();
	wxString fname = plot->file();

	PlotPrintout printout(fname, plot, pointsPerLine, linesPerPage, start, end, dropout, comment, title);
	if (!printout.performPageSetup(true)) {
		return;
	}
	wxPrintDialogData data(printout.getPrintData());
	wxPrinter printer(&data);
	if (!printer.Print(NULL, &printout, true)) {
		wxMessageBox(_("Printing failed"));
	}

}

void PrintDialog::OnPreview(wxCommandEvent& event) {
	int pointsPerLine, linesPerPage, start, end, dropout;
	double spl, istart, iend;
	if (!lineSizeInput->GetValue().ToDouble(&spl) || spl <= 0) {
		wxMessageBox(_("Seconds per line must be a positive number"));
		return;
	}
	if (!startInput->GetValue().ToDouble(&istart) || istart < 0) {
		wxMessageBox(_("Start time must be a non-negative number"));
		return;
	}
	if ((!endInput->GetValue().ToDouble(&iend) || iend <= 0) && endInput->GetValue() != _(STREOF)) {
		wxMessageBox(_("End time must be a positive number"));
		return;
	}
	pointsPerLine = spl * 1000000 / plot->standardFile.interval;
	linesPerPage = pageSizeInput->GetValue();
	start = istart * 1000000 / plot->standardFile.interval;
	if (endInput->GetValue() == _(STREOF))
		end = plot->standardFile.dataPoints.size();
	else
		end = iend * 1000000 / plot->standardFile.interval;
	if (start >= end) {
		wxMessageBox(_("End time must be after start time"));
		return;
	}
	dropout = dropoutInput->GetValue();
	wxString comment = commentInput->GetValue();
	wxString title = titleInput->GetValue();
	wxString fname = plot->file();

	PlotPrintout* printout1 = new PlotPrintout(fname, plot, pointsPerLine, linesPerPage, start, end, dropout, comment, title);
	PlotPrintout* printout2 = new PlotPrintout(fname, plot, pointsPerLine, linesPerPage, start, end, dropout, comment, title);
	if (printout1->performPageSetup(true)) {
		printout2->copyPageSetup(*printout1);
	} else return;

	wxPrintDialogData printData = printout1->getPrintData();
	wxPrintPreview* preview = new wxPrintPreview(printout1, printout2, &printData);
	wxPreviewFrame *frame = new wxPreviewFrame(preview, this, _("Print Preview"));
	frame->Centre(wxBOTH);
	frame->Initialize();
	frame->Show(true);
}

void PrintDialog::OnCancel(wxCommandEvent& event) {
	Show(false);
}
