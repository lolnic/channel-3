// Nicholas Laver
// 14 March 2011
// Preferences Dialogue Box Interface

#ifndef _PREFERENCESWINDOW_
#define _PREFERENCESWINDOW_

#include "channel3.h"
#include "PlotBox.h"
#include "ScaleBar.h"
#include "TabBar.h"
#include "PlotPanel.h"
#include <wx/clrpicker.h>

class PreferencesDialog : public wxFrame{
public:
	PreferencesDialog(wxWindow *parent, PlotPanel* plotPanel, TabBar* tab1, std::vector<wxStaticText*>, wxSlider* slide1);

private:
	PlotPanel* plotPanel;
	wxSlider *slide1;
     TabBar* tab1;
	wxColourPickerCtrl *plotAltPicker, *plotForegroundPicker, *plotBackgroundPicker, *markerPicker, *scaleForegroundPicker, *scaleBackgroundPicker;
	wxCheckBox *plotLinesCheck;
	wxWindow* m_parent;
	wxButton* defaultButton;
	std::vector<wxStaticText*> statics;

	void OnOK(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);
	void OnDefault(wxCommandEvent& event);

	DECLARE_EVENT_TABLE();
};

#endif
