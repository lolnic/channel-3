#ifndef _PLOTBOXBASELINE_
#define _PLOTBOXBASELINE_

class PlotBoxBaseline;

#include "channel3.h"
#include "PlotBox.h"

class PlotBoxBaseline : public PlotBox {
protected:
  void OnPaint(wxPaintEvent& event);
public:
  // Overrided methods
  PlotBoxBaseline(wxWindow* parent, wxWindowID id, wxGLAttributes& args,
      wxPoint pos = wxDefaultPosition,
      wxSize size = wxDefaultSize, long style=0);
  ~PlotBoxBaseline();
};


#endif
