#ifndef _HISTBOX_
#define _HISTBOX_

class HistBox;

#include "channel3.h"
#include "MainWindow.h"
#include "PlotBox.h"

class HistBox : public wxPanel{
	private:
		// Private variables
		PlotBox *plot;
		double scaleMin, scaleMax, frequencyMin, frequencyMax;
		int start, end, binWidth;
		int firstBucket, lastBucket;
		int firstNonZero, lastNonZero;
		int totalFrequency;
		int freqSize;
		int *freq;
		
	public:
		// Overrided methods
		HistBox(wxPanel *parent, int id, 
				wxPoint pos = wxDefaultPosition,
				wxSize size = wxDefaultSize, long style=0);

		// Event handlers
		void OnPaint(wxPaintEvent& event);
		void PaintVertical(wxPaintDC& dc);
		void PaintHorizontal(wxPaintDC& dc);

		// Public methods
		void associatePlotBox(PlotBox *p);
		void writeToFile(wxString filename, wxString mode);
		void generate(double scaleMin, double scaleMax, double frequencyMin, double frequencyMax, int start, int end, int binWidth);
		double division(double size, int& exponent);
		wxString float2str(double number, int exp);
};

#endif
